function addSlideout() {
    container = $("#secondary")
    container.addClass("slideout")
    centerObj = $("#secondary .current")
    if ( centerObj.length ) {
        if ("ontouchstart" in document.documentElement) {
            container.animate({scrollTop: centerObj.offset().top - container.offset().top - container.outerHeight(true) / 2.0}, 500)
        }
        else {
            container.scrollTop(centerObj.offset().top - container.offset().top - container.outerHeight(true) / 2.0)
        }
    }
    $("#secondaryCatchAll").show()
}
function removeSlideout() {
    $("#secondary").removeClass("slideout")
    $("#secondaryCatchAll").hide()
}
function isSlideoutOpen() {
    return $("#secondary").hasClass("slideout")
}

$(document).ready(function () {
    // desktop
    $("#secondary").on("mouseenter", function (e) {
        addSlideout()
        e.preventDefault()
    })
    $("#secondary").on("mouseleave", function (e) {
        removeSlideout()
        e.preventDefault()
    })
    // mobile
    $("#secondary").on("touchstart", function(e) {
        if (! isSlideoutOpen()) {
            addSlideout()
            e.preventDefault()
        }
    })
    $("#secondaryCatchAll").on("touchstart", function(e) {
        removeSlideout()
        e.preventDefault()
    })

    // show hide parts of the nav tree
    _openTreeChar = ">>"
    _closeTreeChar = "<<"
    _openTreeChar = "&#x22EF;" // horizontal ellipsis
    _closeTreeChar = "&#x22ee;" // vertical ellipsis
    _openTreeChar = "&#x2228;" // Logical OR
    _closeTreeChar = "&#x2227;" // Logical AND

    _openTreeChar = "&#x2026;" // normal ellipsis
    _closeTreeChar = "&#x22ee;" // vertical ellipsis

    _openTreeChar = "+"
    _closeTreeChar = "−"

    $("#secondary .treecanhide").each(function (){
        element = $(this)
        if (element.find(".current").length == 0) {
            element.before("<a class=\"showhide\">" + _openTreeChar + "</a>")
            element.toggle()
        }
    })
    $(".showhide").click(function (){
        item = $(this)
        item.next().toggle()
        if (item.next().is(':visible')) {
            item.html(_closeTreeChar)
        }
        else {
            item.html(_openTreeChar)
        }
    })
});
