function getQueryStringValue (key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
options = {}
$(document).ready(function () {
    highlight = getQueryStringValue("highlight")
    $("#main").mark(highlight, options)
    obj = $('mark:visible:first')
    if ( obj.length ) {
        $('html, body').animate({
            scrollTop: obj.offset().top
        }, 1000);
    }
});