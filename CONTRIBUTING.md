# Contributing

see [editing documentation](https://typemytype.gitlab.io/robofont_com/documentation/how-tos/editing-the-docs/)

see [giving feedback on documentation](https://typemytype.gitlab.io/robofont_com/documentation/how-tos/giving-feedback-on-docs/)
