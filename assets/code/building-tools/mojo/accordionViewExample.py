from mojo.UI import AccordionView
from vanilla import *

class AccordionViewExample:

    def __init__(self):

        self.w = FloatingWindow((200, 400))

        self.firstPanel = TextEditor((10, 10, -10, -10))

        self.secondPanel = List((0, 0, -0, -0), ["a", "b", "c"])

        self.thirdPanel = Tabs((10, 10, -10, -10), ["1", "2", "3"])

        self.fourthPanel = Group((0, 0, -0, -0))
        self.fourthPanel.checkBox = CheckBox((10, 10, 100, 22), "CheckBox")
        self.fourthPanel.editText = EditText((10, 40, -10, 22))

        descriptions = [
            dict(label="first panel", view=self.firstPanel, size=200, collapsed=False, canResize=False),
            dict(label="second panel", view=self.secondPanel, minSize=100, size=140, collapsed=True, canResize=True),
            dict(label="third panel", view=self.thirdPanel, minSize=100, size=140, collapsed=True, canResize=False),
            dict(label="fourth panel", view=self.fourthPanel, size=140, collapsed=False, canResize=False)
        ]

        self.w.accordionView = AccordionView((0, 0, -0, -0), descriptions)

        self.w.open()

AccordionViewExample()
