from random import randint

glyph = CurrentGlyph()
glyph.prepareUndo('move points randomly')

# iterate over all points in glyph
for contour in glyph:
    for point in contour.points:
        # move points by a random distance
        point.x += randint(-20, 20)
        point.y += randint(-20, 20)

glyph.changed()
glyph.performUndo()
