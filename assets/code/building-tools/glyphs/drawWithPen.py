# get a font
f = CurrentFont()

# get a glyph
g = f['a']

# empty the glyph
g.clear()

# get a pen to draw in the glyph
pen = g.getPen()

# draw a closed Bezier path
pen.moveTo((0, 0))
pen.lineTo((0, 200))
pen.curveTo((50, 150), (150, 150), (200, 200))
pen.lineTo((200, 0))
pen.closePath()

# get another glyph
g2 = f['b']
g2.clear()

# draw an open Bezier path
pen = g2.getPen()
pen.moveTo((0, 0))
pen.lineTo((200, 100))
pen.lineTo((0, 200))
pen.lineTo((200, 300))
pen.endPath()
