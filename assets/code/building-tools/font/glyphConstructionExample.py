from glyphConstruction import *

txt = '''\
agrave = a + grave@center,top
aacute = a + acute@center,top
'''

constructions = ParseGlyphConstructionListFromString(txt)

font = CurrentFont()

for construction in constructions:
    constructionGlyph = GlyphConstructionBuilder(construction, font)
    glyph = font.newGlyph(constructionGlyph.name)
    constructionGlyph.draw(glyph.getPen())
    glyph.name = constructionGlyph.name
    glyph.unicode = constructionGlyph.unicode
    glyph.note = constructionGlyph.note
    glyph.markColor = constructionGlyph.mark
    glyph.width = constructionGlyph.width
