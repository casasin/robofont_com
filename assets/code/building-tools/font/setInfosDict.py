# a dictionary with font infos
infoDict = {
    # attribute name (key) : data (value)
    'openTypeNameDesigner' : 'Claude Garamond',
    'year' : 1532,
    'copyright' : 'Copyright (c) 1532 Claude Garamond.',
    'note' : 'bonjour le monde',
}

# for each open font
for font in AllFonts():
    # iterate over all font info attributes in dict
    for key in infoDict.keys():
        # set data in font info attribute
        setattr(font.info, key, infoDict[key])
# done!
