# open a font without UI
srcFont = OpenFont(showInterface=False)

# get the current font
f = CurrentFont()

# create a layer name based on the familyName and styleName of the source font
layerName = "%s %s" % (srcFont.info.familyName, srcFont.info.styleName)

# loop over all glyphs in the source font
for g in srcFont:
    # if the glyph doesn't exist in the current font, create a new glyph
    if g.name not in f:
        f.newGlyph(g.name)
    # get the layered glyph
    layerGlyph = f[g.name].getLayer(layerName)

    # get the point pen of the layer glyph
    pen = layerGlyph.getPointPen()
    # draw the points of the imported glyph into the layered glyph
    g.drawPoints(pen)

# we are done :)
print("done")
