# get current font
f = CurrentFont()

# a dictionary of glyph construction recipes
accentsDict = {
    # accented glyph (key) : [base glyph, [(accent, anchor)]], (value)
    'Aacute' : ['A', [('acute', 'top')]],
    'aacute' : ['a', [('acute', 'top')]],
    # ...add more accented glyphs here...
}

# iterate over accented glyphs
for accentedGlyph in accentsDict.keys():
    # get base glyph and accents+anchors
    baseGlyph, accents = accentsDict[accentedGlyph]
    # build accented glyph from parts
    f.compileGlyph(accentedGlyph, baseGlyph, accents)
