# define width value
value = 400

# get current font
font = CurrentFont()

# iterate over all glyphs in font
for glyph in font:
    # set glyph width to value
    glyph.width = value
