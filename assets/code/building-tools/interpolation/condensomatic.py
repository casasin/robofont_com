# a sample font containing Regular and Bold versions of a glyph
f = CurrentFont()

# Regular and Bold glyph names
regular = f['regular']
bold = f['bold']

# measure the stem widths for each weight
regularStem = 70
boldStem = 170

# condensing factor
condenseFactor = 0.2

# calculate scale factor from Regular and Bold stem widths
xFactor = float(regularStem) / (regularStem + condenseFactor * (boldStem - regularStem))

# create a new glyph for the result
g = f.newGlyph('condensed')
g.markColor = 1, 1, 0, 0.7

# interpolate weights
g.interpolate((condenseFactor, 0), regular, bold)

# scale glyph horizontally
g.scaleBy((xFactor, 1))

# calculate glyph margins
g.leftMargin = (regular.leftMargin + bold.leftMargin) * 0.5 * (1.0 - condenseFactor)
g.rightMargin = (regular.rightMargin + bold.rightMargin) * 0.5 * (1.0 - condenseFactor)
