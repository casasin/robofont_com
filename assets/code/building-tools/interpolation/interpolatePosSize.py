# define position and size for two rectangles
x1, y1 = 98, 88
w1, h1 = 480, 492

x2, y2 = 912, 794
w2, h2 = 20, 126

# total amount of steps
steps = 22

# define blend mode and color
blendMode('multiply')
fill(0, 0.3, 1, 0.2)

# iterate over the total amount of steps
for i in range(steps):

    # calculate interpolation factor for this step
    factor = i * 1.0 / (steps - 1)

    # interpolate each rectangle attribute separately
    x = x1 + factor * (x2 - x1)
    y = y1 + factor * (y2 - y1)
    w = w1 + factor * (w2 - w1)
    h = h1 + factor * (h2 - h1)

    # draw a rectangle with the calculated variables
    rect(x, y, w, h)
