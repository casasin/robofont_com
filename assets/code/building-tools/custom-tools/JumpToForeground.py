from mojo.events import addObserver
from mojo.UI import SetCurrentLayerByName

class JumpToForeground():

    def __init__(self):
        addObserver(self, "keyWasPressed", "keyDown")

    def keyWasPressed(self, info):
        event = info["event"]
        characters = event.characters()
        if characters == "f":
            SetCurrentLayerByName("foreground")

JumpToForeground()