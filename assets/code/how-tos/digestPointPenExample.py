from fontPens.digestPointPen import DigestPointPen

f = CurrentFont()

myPen = DigestPointPen()
f['period'].drawPoints(myPen)

print(myPen.getDigest())
