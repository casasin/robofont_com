import variableFontGenerator

desingSpacePath = 'myMutatorSansFolder/MutatorSans.designspace'
varFontPath = 'myMutatorSansFolder/MutatorSans.ttf'

p = variableFontGenerator.BatchDesignSpaceProcessor(desingSpacePath)
p.generateVariationFont(varFontPath)
