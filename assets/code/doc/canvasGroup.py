from mojo.canvas import CanvasGroup
from mojo.drawingTools import *
from vanilla import Window

class ExampleWindow:

    def __init__(self):
        self.w = Window((400, 400), minSize=(200, 200))
        self.w.canvas = CanvasGroup((0, 0, -0, -0), delegate=self)
        self.w.open()

    def draw(self):
        rect(10, 10, 100, 100)

ExampleWindow()