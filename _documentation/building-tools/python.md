---
layout: page
title: Introduction to Python
tree:
  - introduction
  - uses
  - terminal
  - robofont
  - drawbot
  - language
  - numbers-arithmetics
  - variables
  - strings
  - lists
  - tuples
  - sets
  - collections-loops
  - comparisons
  - conditionals
  - boolean-logic
  - dictionaries
  - builtin-functions
  - functions
  - objects
  - standard-modules
  - external-modules
  - custom-modules
treeCanHide: true
level: beginner
---

This section provides a quick introduction to the Python programming language. It covers the basics you need to know to get started with scripting in RoboFont.

{% tree page.url levels=3 %}

> - {% internallink "python/resources" %}
{: .seealso }

> This chapter is being updated to Python 3.
{: .warning }
