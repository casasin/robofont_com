---
layout: page
title: Scripts to do things to fonts
tags:
    - scripting
    - FontParts
level: intermediate
---

* Table of Contents
{:toc}

## Set fixed width in all glyphs

{% showcode building-tools/font/makeMonowidth.py %}

## Set infos in all open fonts

This example shows how to set font infos from a dictionary into all open fonts.

{% showcode building-tools/font/setInfosDict.py %}

## Copy font infos from one font to another

This example shows how to copy font info data from one open font to another.

{% showcode building-tools/font/copyFontInfo.py %}

## Generate fonts for all UFOs in folder

This script will first open a dialog for selecting a folder; it will then open each UFO file in the folder, without the UI, and generate an OpenType-CFF font for it.

{% showcode building-tools/font/batchGenerateFonts.py %}

## Import a font into layer of current font

This script shows how to import all glyphs of a font into a layer of the current font. Notice how the source font is never opened in the UI: it is just loaded in memory. Note also how the glyphs are copied from one font to the other with a *point pen*.

{% showcode building-tools/font/importFontIntoLayer.py %}

<!--
## Build accented glyphs

This script shows how to build accented glyphs from a dictionary of glyph construction recipes. It assumes that all base glyphs and accented glyphs have correctly named anchors.

{% showcode building-tools/font/buildAccentedGlyphs.py %}
-->
