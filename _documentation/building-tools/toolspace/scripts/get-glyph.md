---
layout: page
title: How to get a glyph
tags:
    - FontParts
level: beginner
---

There are different ways to get to glyphs. We’ve already seen how to ask a font for a glyph, by its name:

``` python
font = CurrentFont()
glyph = font['A']
print(glyph)
```

``` console
>>> <RGlyph at 4724219600>
```

We can also tell the font object to create a new glyph with a given name:

    f.newGlyph('hello')

If the new glyph already exists in the font, it will be overwritten.

## CurrentGlyph

In a way similar to `CurrentFont`, we can also ask RoboFont for the current glyph:

``` python
glyph = CurrentGlyph()
print(glyph)
```

``` console
>>> <RGlyph at 4663441680>
```

This will return the glyph which is currently open in the Glyph Window. If no Glyph Window is open, and a glyph is selected in the Font Window, it will return that glyph. If no Glyph Window is open, and no glyph or several glyphs are selected in the Font Window, it will return `None`.

## RGlyph

In some cases, we might want to create a glyph 'in space', outside of any font – as an intermediate step for transformations, for example. The `RGlyph` object can be used for this:

    glyph = RGlyph()

    # do something to the glyph...

    # insert glyph in current font
    font.insertGlyph(glyph, name='hi')
