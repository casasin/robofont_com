---
layout: page
title: The RoboFont scripting environment
treeTitle: Scripting environment
tags:
  - scripting
  - FontParts
level: beginner
---

* Table of Contents
{:toc}

## Scripting Window

RoboFont’s primary scripting environment is the {% internallink "workspace/scripting-window" %}, a code editor which allows you to write, open, edit and run Python scripts.

{% image workspace/scripting-window.png %}

The output from scripts which are run in the Scripting Window appear in the bottom part of the window.

{% internallink "workspace/scripting-window#script-browser" text='Script browser' %}
: The Scripting Window includes a file browser for navigating through a folder of scripts. It can be turned on/off by clicking on the icon at the bottom left. Scripts in the current scripts folder are listed under the *Scripts* menu.

{% internallink "workspace/scripting-window#code-interaction" text='Code interaction' %}
: The code editor supports a special interaction mode which allows the user to modify selected values dynamically.

### Font objects out-of-the-box

When writing code in the Scripting Window, the main {% internallink 'building-tools/toolkit/fontparts' text='font objects' %} are available out of the box – so you can use `CurrentFont`, `OpenFont`, `NewFont`, `AllFonts`, `CurrentGlyph` etc. directly, without the need to import them at the top of your scripts.

```python
print(CurrentFont())
```

```console
<RFont 'RoboType Roman' at 4864862160>
```

```python
print(CurrentGlyph())
```

```console
<RGlyph 'G' ('foreground') at 4872730960>
```

Note that these objects are available only to scripts which are run in the Scripting Window. If your main script calls a function from an external module, you’ll need to import the font objects explicitly from `mojo.roboFont` in that file:

```python
# getFont.py
from mojo.roboFont import CurrentFont

def getFont():
    return CurrentFont()
```

```python
from getFont import getFont

print(getFont())
```

```console
<RFont 'RoboType Roman' at 4853462928>
```

### Menu title and keyboard shortcut

RoboFont supports a custom title and a keyboard shortcut for each script. When available, the title is displayed in the *Scripts* menu instead of the file name.

Title and shortcut must be added at the top of the script using the following syntax:

```python
# menuTitle : my funky script
# shortCut  : command+shift+alt+t

print('hello world')
```

Shortcuts must be constructed using the following format:

```text
command+shift+control+alt+<input>
```

Not all modifier keys are required.

The input key can be a character or any of the following keys:

```text
space tab backtab arrowup arrowdown arrowleft arrowright f1 f2 f3 f4 f5 f6 f7 f8 f9 f10 f11 f12 f13 f14 f15 f16 f17 f18 f19 f20 f21 f22 f23 f24 f25 f26 f27 f28 f29 f30 f31 f32 f33 f34 f35 enter backspace delete home end pageup pagedown
```

## Output Window

The {% internallink "workspace/output-window" %} is also important while developing your tools or debugging someone else’s tools: this is where print statements and tracebacks appear when a script is not running from the Scripting Window.

{% image building-tools/output-window.png %}

