---
layout: page
title: Proof spacing
tags:
  - scripting
  - drawBot
level: intermediate
---

This script uses an installed font to set text, rather than displaying single glyphs from the UFO directly.

A test version of your font can be installed using the `font.testInstall()` function. (This line is commented-out because you don’t need to generate and install a font every time you run the script.)

The script generates spacing test strings for lowercase, uppercase, digits and punctuation characters. The list of characters in each group is imported from the `string` module – you could easily extend it with additional glyphs which are present in your font.

{% image building-tools/drawbot/proof-spacing.png %}

{% showcode building-tools/drawbot/proofSpacing.py %}
