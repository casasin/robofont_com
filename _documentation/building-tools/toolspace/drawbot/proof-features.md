---
layout: page
title: Proof OpenType features
tags:
  - scripting
  - drawBot
level: intermediate
---

This script shows how to ask a font for its features, and how to turn OpenType features on/off.

{% showcode building-tools/drawbot/proofOpenTypeFeatures.py %}
