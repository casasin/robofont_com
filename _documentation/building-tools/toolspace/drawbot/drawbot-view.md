---
layout: page
title: Using DrawBot with vanilla
tags:
  - scripting
  - drawBot
level: intermediate
---

*DrawBot can also be used together with vanilla!*

This means that you can integrate a DrawBot-view inside your dialogs and tools. Or add an interface around your DrawBot scripts.

{% image building-tools/drawbot/drawbot-vanilla.png %}

{% showcode building-tools/drawbot/simpleDrawBotUIExample.py %}
