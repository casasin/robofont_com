---
layout: page
title: Tools with Observers
tree:
  - the-observer-pattern
  - event-observer
  - list-layers
  - custom-glyph-preview
  - show-glyph-box
  - draw-reference-glyph
tags:
  - observers
treeCanHide: true
level: advanced
---

In all UI examples we’ve seen so far, objects and interface communicate directly using callbacks. This works great for simple tools. If you are a designer building small tools for yourself, there’s a good chance that you’ll never need to go beyond that.

This section and the next are for developers interested in building more complex UIs and applications using the *observer pattern*. RoboFont itself is built using events and observers, and you can take advantage of that when building your own custom tools.

**The Observer pattern adds a layer of abstraction between objects and interface, resulting in cleaner, more flexible code.**

{% tree page.url levels=1 %}

> - {% internallink 'how-tos/building-a-custom-key-observer' %}
> - [RoboFont Forum > observers](http://forum.robofont.com/tags/observers)
{: .seealso }

{% comment %}
- Draw reference glyph
- Simple draw observer
- Simple window observer
- Reorder contours
- Show distance between selected points
- Stencil preview
- View glyphs in current glyph mask
- Preview expanded strokes
- Display point count
{% endcomment %}
