---
layout: page
title: Introduction to vanilla
tags:
    - vanilla
level: beginner
---

vanilla is Python wrapper around Apple’s native Cocoa UI layer. It offers a pythonic API for creating interfaces with code, rather than using visual tools like Apple’s Interface Builder.

<!-- vanilla was developed by Tal Leming, and was inspired by the classic W library used by RoboFog. W was a Python wrapper around the native UI layer of the Classic Mac OS. -->

> - [Tal Leming introducing vanilla (RoboThon 2012)](http://vimeo.com/116064787#t=17m00s)
{: .seealso}

## Using vanilla

vanilla is embedded in RoboFont, so you can start using it right away:

    from vanilla import *

[vanilla]: http://ts-vanilla.readthedocs.io/

vanilla includes very complete documentation for all its objects, available directly from your scripting environment:

```python
from vanilla import *

help(Window)
```

The above line will return the full inline documentation for the `Window` object, including some sample code:

```python
from vanilla import *

class WindowDemo(object):

    def __init__(self):
        self.w = Window((200, 70), "Window Demo")
        self.w.myButton = Button((10, 10, -10, 20), "My Button")
        self.w.myTextBox = TextBox((10, 40, -10, 17), "My Text Box")
        self.w.open()

WindowDemo()
```

If you run the above code, you will get this simple demo window:

{% image building-tools/vanilla/WindowDemo.png %}

## vanilla overview

Below is an overview of all UI elements included in vanilla. These images are simple screenshots from vanilla’s own test files, which are included in the library.

```python
from vanilla.test.testAll import Test
Test()
```

{% image building-tools/vanilla/windows.png caption='windows' %}

{% image building-tools/vanilla/text.png caption='text objects' %}

{% image building-tools/vanilla/buttons.png caption='buttons, sliders, etc.' %}

{% image building-tools/vanilla/views.png caption='views' %}

{% image building-tools/vanilla/toolbar.png caption='toolbar' %}

> The List test crashes RoboFont.
{: .todo }
