---
layout: page
title: Simple font library browser
tags:
    - scripting
    - vanilla
level: intermediate
---

Imagine that you have several type-families in development. The UFO source files are kept in separate subfolders of a main folder, each named after the family it contains.

{% image building-tools/fontsFolder.png %}

This example shows a simple dialog for browsing through the family folders, and makes it easy to open selected fonts.

{% image building-tools/FontLibraryBrowser.png %}

The code could be extended to do more than just opening the selected fonts: setting font infos, generating fonts etc.

{% showcode /building-tools/vanilla/simpleFontLibraryBrowser.py %}
