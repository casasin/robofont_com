---
layout: page
title: Simple dialog with action buttons
tags:
    - scripting
    - vanilla
level: intermediate
---

This example shows a very simple tool with a dialog.

The dialog contains two buttons, and each button triggers an action:

- the fist button prints the names of the selected glyphs
- the second button paints the glyph cells of the selected glyphs

{% image building-tools/ToolDemo.png %}

This example is useful to show some common coding patterns:

1. Callbacks

    > A *callback* is a function that is called by another function which takes the first function as a parameter. Usually, a callback is a function that is called when something happens. That something can be called an *event* in programmer-speak. ([source])

    [source]: http://stackoverflow.com/questions/9596276/how-to-explain-callbacks-in-plain-english-how-are-they-different-from-calling-o#9652434

    In our example, the event is the clicking of a button. When that event is triggered, the dialog will run the specified function (the callback).

    Callbacks are used extensively in `vanilla`.

2. Defensive programming

    The tool does something to the selected glyphs in the current font. But what if no glyph is selected, or there is no font open? A well-written tool should handle such situations elegantly, avoiding errors and providing informative messages to the user.

    Notice how, in the code below, the program exits the function if these two conditions – a font is open, and at least one glyph is selected – are not met.

3. Prepare Undo & Perform Undo

    If a tool makes changes to user data, it should also make it possible for the user to *revert* these changes if necessary.

    In RoboFont, this is accomplished by recording the state of the data *before* and *after* the change is made – look for `glyph.prepareUndo()` and `glyph.performUndo()` in the code below.

{% showcode /building-tools/vanilla/simpleToolDemo.py %}

> In RoboFont 1.8, use `glyph.mark` instead of `glyph.markColor`.
{: .note }
