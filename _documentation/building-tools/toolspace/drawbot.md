---
layout: page
title: Scripts using DrawBot
tags:
    - scripting
    - drawBot
tree:
    - proof-character-set
    - proof-glyphs
    - proof-spacing
    - proof-features
    - drawbot-script
    - drawbot-view
treeCanHide: true
level: intermediate
---

While drawing a typeface, it is important to see how things look like in context – to evaluate shape consistency, rhythm, color, spacing etc.

[DrawBot] is an open-source Python library for generating graphics. It is not embedded in RoboFont, but it can be easily installed with the [DrawBot extension]. The extension allows you to use drawBot from within RoboFont, with full access to the RoboFont API – so you can use `CurrentFont()` or `CurrentGlyph()` directly in your DrawBot scripts.

The extension also installs drawBot as module, so you can import and use it in your own code, without the UI.

**DrawBot allows you to generate all kinds of proofs and text samples from within RoboFont.**

[DrawBot]: http://www.drawbot.com/
[DrawBot extension]: http://github.com/typemytype/drawBotRoboFontExtension

## Examples

{% tree page.url levels=1 %}
