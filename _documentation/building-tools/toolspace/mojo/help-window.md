---
layout: page
title: Help Window
tags:
  - mojo
level: intermediate
---

The Help window is a simple HTML browser, useful for viewing documentation pages.

{% image building-tools/HelpWindow.png %}

```python
from mojo.UI import HelpWindow

HelpWindow("http://robofont.com")
```
