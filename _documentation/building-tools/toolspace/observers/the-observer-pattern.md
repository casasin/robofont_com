---
layout: page
title: The Observer Pattern
tags:
  - scripting
  - observers
level: advanced
---

{% comment %}
Explain what the Observer Pattern is and how it works.
{% endcomment %}

RoboFont is designed around a model where most actions – such as opening a font, typing a keystroke, toggling on a glyph preview – will all broadcast “notifications” to any Python script that wants to listen in.

This system of notifications is what makes more complex extensions possible, but they’re even handy to add your own “preferences” to RoboFont, adding features that the application developer couldn’t have anticipated someone else needing.

> By running the {% internallink "toolspace/observers/event-observer" text="Event Observer extension" %} you can see all of the notifications flooding in as you work – opening a new font might trigger more than 10 notifications for each action that takes place behind the scenes.
{: .tip }

> - [Tal Leming: Building Apps (RoboThon 2012)] + [partial transcript]
{: .seealso }

[Tal Leming: Building Apps (RoboThon 2012)]: http://vimeo.com/116064787#t=4m30s
[partial transcript]: http://gist.github.com/gferreira/18b9af056a9d4319d207d5a6c7ee7b2c
