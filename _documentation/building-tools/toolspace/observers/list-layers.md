---
layout: page
title: List font layers
tags:
    - observers
level: advanced
---

A very simple example tool: a dialog which displays a list of layers in the current font. If the current font changes, the font name and layers list in the UI are updated automatically.

{% image building-tools/ListLayersTool.png %}

{% showcode building-tools/custom-tools/listLayersTool.py %}

> - load font name and layers when the dialog is opened
> - remove font name and layers when all fonts are closed
{: .todo }
