---
layout: page
title: Publishing extensions on Mechanic 2
treeTitle: Publishing on Mechanic 2
tags:
  - extensions
  - Mechanic
level: intermediate
---

* Table of Contents
{:toc}

## Overview

**The process of publishing an extension on Mechanic 2 is all done using Git and [GitHub].**

In Mechanic 2, extension metadata is stored centrally in the [Mechanic 2 server].

The server contains a folder named `_data` where metadata files for single extensions are stored. Extensions are published by adding new files to this folder.

{% image building-tools/extensions/publishing-extensions_data-folder.png %}

> In the first version of Mechanic, extension metadata was stored *locally* (inside each extension) in the extension’s `info.plist` file – using the custom key `com.robofontmechanic.mechanic`. This custom key is now deprecated and can be deleted – see the updated {% internallink 'building-tools/extensions/extension-file-spec' %}.
{: .note }

## Extension metadata file

Before you can publish your extension in Mechanic 2, you’ll first need to create an *extension metadata file* for it. See {% internallink 'extensions/extension-item-format' %} for details.

## Publishing your extension

> A [GitHub] account is required in order to fork, modify and submit changes back to the main repository.
{: .note }

1. Fork the [Mechanic 2 server] repository.
2. Add an `<extensionName>.yml` file to the `_data` folder.
3. Make a pull request, and wait until it is approved by one of the admins.

After the changes are merged, your extension will appear on the [Mechanic 2 website] and will become available to users of the [Mechanic 2 extension].

[GitHub]: http://github.com/
[Mechanic 2 server]: http://github.com/robofont-mechanic/mechanic-2-server/
[Mechanic 2 website]: http://robofontmechanic.com/
[Mechanic 2 extension]: http://github.com/robofont-mechanic/mechanic-2
