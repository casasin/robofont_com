---
layout: page
title: Boilerplate RoboFont Extension
treeTitle: Boilerplate Extension
tags:
  - extensions
level: intermediate
---

The [Boilerplate Extension] is a dummy RoboFont extension which doesn’t do anything useful. Its purpose is to serve as a template for users to get started building their own extensions. It is hosted on GitHub, so anyone can fork it and make changes.

[Boilerplate Extension]: http://github.com/roboDocs/rf-extension-boilerplate

Looking at the source code of extensions by different developers, we can find {% internallink 'building-tools/extensions/packaging-patterns' text='different ways to organize files' %}. The Boilerplate Extension demonstrates a simple scheme in which the source files and the extension live side-by-side in the same repository:

> rf-extension-boilerplate
> ├── source/
> │   ├── code/
> │   ├── documentation/
> │   └── resources/
> ├── build/myExtension.roboFontExt
> │   ├── lib/
> │   ├── html/
> │   ├── resources/
> │   ├── info.plist
> │   └── license
> ├── README.md
> ├── build.py
> ├── license.txt
> ├── myExtension.mechanic
> └── myExtensionMechanicIcon.png
{: .asCode }

`source/`
: contains the ‘master files’ which are edited by the developer

`build/myExtension.roboFontExt`
: the generated extension which is distributed to users

`build.py`
: a script to generate the extension package from the source files

`README.md`
: a [markdown] file with information about the repository

`license.txt`
: defines the terms under which the source code is made available

`myExtension.yml`
: a [yaml] file with metadata for installation with Mechanic

`myExtensionMechanicIcon.png`
: an icon to identify the extension in the Mechanic extension index

[yaml]: http://yaml.org/
[markdown]: http://en.wikipedia.org/wiki/Markdown
