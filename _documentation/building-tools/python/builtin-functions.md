---
layout: page
title: Built-in functions
level: beginner
---

Python comes with many built-in functions which are always available as part of the language. We’ve already seen some of them in the previous sections. This page gives an overview of the most important ones.

> - [Built-in Functions](http://docs.python.org/3.6/library/functions)
{: .seealso }

* Table of Contents
{:toc}

## Type conversions

The `type()` function returns the type of a given object:

```python
>>> type('hello')
```

```console
<type 'str'>
```

```python
>>> type(1)
```

```console
<type 'int'>
```

```python
>>> type(33.3)
```

```console
<type 'float'>
```

Python has several built-in functions to convert from one data type to another. These functions have the same name as the data types, so it’s easy to remember them:

### str()

The `str()` function transforms something into a string, or creates a new empty string if called without any argument:

```python
>>> a = str(10)
>>> a, type(a), len(a)
```

```console
('10', <class 'str'>, 2)
```

```python
>>> b = str()
>>> b, type(b), len(b)
```

```console
('', <class 'str'>, 0)
```

### list()

The `list()` function transforms something into a list, or creates a new list if called without any argument:

```python
>>> list('abcdefghijklmn')
```

```console
['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n']
```

```python
>>> L = list()
>>> L, type(L), len(L)
```

```console
([], <class 'list'>, 0)
```

### dict()

The `dict()` function can create a dictionary from different types of arguments, or a new empty dictionary if called without any arguments.

Here’s how we can create a new dictionary from a list of keyword/value tuples:

```python
>>> dict([('hello', 1), ('world', 2)]
```

```console
{'hello': 1, 'world': 2}
```

And here’s how we can create a new dictionary using keyword arguments:

```python
dict(hello=1, world=2)
```

```console
{'hello': 1, 'world': 2}
```

When called without any argument, `dict()` creates a new empty dictionary:

```python
>>> d = dict()
>>> type(d), len(d), d
```

```console
(<class 'dict'>, 0, {})
```

### float()

The `float()` function transforms an integer or string into a floating point (decimal) number:

```python
>>> float(27)
```

```console
27.0
```

When called without any argument, `float()` creates a new float with value `0.0`:

```python
>>> float()
```

```console
0.0
```

Conversion from string to float only works if the string looks like a number:

```python
>>> float('27.0')
```

```console
27.0
```

```python
>>> float('27')
```

```console
27.0
```

```python
>>> float('27,0')
```

```console
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ValueError: could not convert string to float: '27,0'
```

### bool()

The `bool()` function transforms any data type into a boolean value:

```python
>>> bool(1)
```

```console
True
```

```python
>>> bool('')
```

```console
False
```

When called without any argument, `bool()` returns `False`:

```python
>>> bool()
```

```console
False
```

> See {% internallink "python/comparisons#testing-truthiness" text="testing truthiness" %} for examples of conversions from other data types.
{: .seealso }

### set()

The `set()` function creates a new set object from a collection:

```python
>>> set('abracadabra')
```

```console
{'r', 'b', 'c', 'a', 'd'}
```

```python
>>> set([1, 5, 10, 50, 10, 5, 1])
```

```console
{1, 10, 50, 5}
```

```python
>>> set({'a': 5, 'b': 3, 'c': 1})
```

```console
{'c', 'a', 'b'}
```

```python
>>> set(((10, 20), (20, 25), (10, 20)))
```

```console
{(20, 25), (10, 20)}
```

When called without any argument, `set()` creates a new empty set:

```python
>>> set()
```

```console
set()
```

### tuple()

The `tuple()` function creates a tuple from a collection:


```python
tuple([1, 2, 3])
```

```console
>>> (1, 2, 3)
```

```python
>>> tuple('abc')
```

```console
('a', 'b', 'c')
```

```python
>>> tuple({'a': 5, 'b': 3, 'c': 1})
```

```console
('a', b', 'c')
```

When called without any argument, `tuple()` creates a new empty tuple:

```python
>>> tuple()
```

```console
()
```

## Collections and loops

### len()

The `len()` function returns the amount of items in a collection:

```python
>>> L = ['A', 'B', [1, 2, 3], 'D']
>>> len(L)
```

```console
4
```

```python
>>> len('I will not buy this record, it is scratched.')
```

```console
44
```

### range()

The `range()` function returns an iterator which yields a sequence of numbers. It is typically used to repeat an action a number of times:

```python
>>> range(0, 4)
```

```console
range(0, 4)
```

```python
>>> for i in range(4):
...     i
```

```console
0
1
2
3
```

### enumerate()

The `enumerate()` function returns an iterator which adds a counter to each item in a collection:

```python
>>> L = ['Alpha', 'Bravo', 'Charlie', 'Delta']
>>> enumerate(L)
```

```console
<enumerate object at 0x1039f6558>
```

```python
>>> list(enumerate(L))
```

```console
[(0, 'Alpha'), (1, 'Bravo'), (2, 'Charlie'), (3, 'Delta')]
```

The `enumerate()` function is typically used to loop over the items in a collection:

```python
>>> for index, item in enumerate(L):
...     index, item
```

```console
(0, 'Alpha')
(1, 'Bravo')
(2, 'Charlie')
(3, 'Delta')
```

### sorted()

The `sorted()` function returns a sorted copy of a given collection. This is useful if we need to do a sorted loop without actually sorting the collection:

```python
>>> L = ['Mike', 'Bravo', 'Charlie', 'Tango', 'Alpha']
>>> for item in sorted(L):
...     item
```

```console
'Alpha'
'Bravo'
'Charlie'
'Mike'
'Tango'
```

### reversed()

The `reversed()` function is similar to `sorted()`: it returns a copy of a given collection with the items in inverse order.

```python
>>> for item in reversed(L):
...     item
```

```console
'Alpha'
'Tango'
'Charlie'
'Bravo'
'Mike'
```

### zip()

The `zip()` function takes two separate lists, and produces a new list with pairs of values (one from each list):

```python
>>> L1 = ['A', 'B', 'D', 'E', 'F', 'G', 'H', 'I']
>>> L2 = [1, 2, 3, 4, 5, 6]
>>> zip(L1, L2)
```

```console
<zip object at 0x1039f8608>
```

```python
>>> list(zip(L1, L2))
```

```console
[('A', 1), ('B', 2), ('D', 3), ('E', 4), ('F', 5), ('G', 6)]
```

The resulting list has the same amount of items as the smallest of the two lists.

The `zip` function can be used to create a dictionary from a list of keys and a list of values:

```python
>>> D = dict(zip(L1, L2))
>>> D
```

```console
{'A': 1, 'B': 2, 'D': 3, 'E': 4, 'F': 5, 'G': 6}
```

### map()

...


### filter()

...

## Numerical

Some built-in functions are useful when working with numbers:

### abs()

The `abs()` function returns the absolute value of a number:

```python
>>> abs(-100)
```

```console
100
```

### sum()

The `sum()` function returns the sum of all numbers in a list:

```python
>>> L = [10, 300.7, 50, 33.1]
>>> sum(L)
```

```console
393.8
```

### min() and max()

The `min()` function returns the smallest number in a list of numbers:

```python
>>> min(L)
```

```console
10
```

The `max()` function returns the largest number in a list of numbers:

```python
>>> max(L)
```

```console
300.7
```

### round()

The `round()` function rounds a number to a given precision.

When called without any argument, `round()` converts a float to the nearest integer:

```python
>>> round(3.1416)
```

```console
3
```

If two multiples are equally close, rounding is done toward the even number:

```python
>>> round(0.5)
```

```console
0
```

```python
>>> round(1.5)
```

```console
2
```

The `round()` function can also take a second argument to specify the amount of digits to round after the decimal point:

```python
>>> round(3.141592653589793, 4)
```

```console
3.1416
```

## Boolean

### any()

The built-in function `any()` evaluates all statements in a collection and returns:

- `True` if at least one statement is `True`
- `False` if all statements are `False`

```python
>>> any([0, 0, 0, 0])
```

```console
False
```

```python
>>> any([0, 0, 1, 0])
```

```console
True
```

### all()

The built-in function `all()` evaluates all statements in a collection, and returns:

- `True` if all statements are `True`
- `False` if at least one statement is `False`

```python
>>> all([0, 0, 0, 0])
```

```console
False
```

```python
>>> all([0, 0, 1, 0])
```

```console
False
```

```python
>>> all([1, 1, 1, 1])
```

```console
True
```

## Attributes

### dir()

The `dir()` function returns a list of names (attributes and methods) in a given object. This is useful to get an idea of how an object is structured and what it can do.

Here’s an example using `dir()` to view the contents of an imported module:

```python
>>> import math
>>> dir(math)
```

```console
['__doc__', '__file__', '__loader__', '__name__', '__package__', '__spec__', 'acos', 'acosh', 'asin', 'asinh', 'atan', 'atan2', 'atanh', 'ceil', 'copysign', 'cos', 'cosh', 'degrees', 'e', 'erf', 'erfc', 'exp', 'expm1', 'fabs', 'factorial', 'floor', 'fmod', 'frexp', 'fsum', 'gamma', 'gcd', 'hypot', 'inf', 'isclose', 'isfinite', 'isinf', 'isnan', 'ldexp', 'lgamma', 'log', 'log10', 'log1p', 'log2', 'modf', 'nan', 'pi', 'pow', 'radians', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'tau', 'trunc']
```

When called without any argument, `dir()` returns a list of names in the current local scope.

If you’re starting a new interactive session in Terminal, `dir()` will return only *private* attributes and methods (with names starting with double underscores) which are made available by the Python language:

```python
>>> dir()
```

```console
['__annotations__', '__builtins__', '__doc__', '__loader__', '__name__', '__package__', '__spec__']
```

Variable names declared during the session are added to the local scope, and are included in `dir()` results too:

```python
>>> 'myVariable' in dir()
```

```console
False
```

```python
>>> myVariable = 120
>>> 'myVariable' in dir()
```

```console
True
```

If you are scripting in RoboFont, you can use `dir()` to see that all the main [fontParts.world] objects are available out of the box in the {% internallink 'workspace/scripting-window' %}:

[fontParts.world]: http://fontparts.readthedocs.io/en/latest/objectref/fontpartsworld/index.html

```python
print(dir())
```

```console
['AllFonts', 'CreateCursor', 'CurrentAnchors', 'CurrentComponents', 'CurrentContours', 'CurrentFont', 'CurrentGlyph', 'CurrentGuidelines', 'CurrentLayer', 'CurrentPoints', 'CurrentSegments', 'FontsList', 'NewFont', 'OpenFont', 'OpenWindow', 'RAnchor', 'RComponent', 'RContour', 'RFeatures', 'RFont', 'RGlyph', 'RGroups', 'RInfo', 'RKerning', '__builtins__', '__file__', '__name__', 'help']
```

### getattr()

The `getattr()` function is used to get an attribute from a given object. Instead of accessing the attribute directly, we pass the object and the attribute name to the `getattr()` function, and get a value back. This ‘indirect’ approach allows us to write code that is more abstract and more reflexive.

Here’s a simple example using an open font in RoboFont. Let’s imagine that we want to print out some font info attributes. We could access them directly one by one, like this:

```python
font = CurrentFont()
print(font.info.familyName)
print(font.info.styleName)
print(font.info.openTypeNameDesigner)
```

```console
RoboType
Roman
Frederik Berlaen
```

Using `getattr()`, we can separate the definition of the attribute names from the retrieval of the values:

```python
attrs = ['familyName', 'styleName', 'openTypeNameDesigner']
for attr in attrs:
    print(getattr(font.info, attr))
```

### setattr()

The `setattr()` function is complementary to `getattr()` – it allows us to set the value of an object’s attribute.

Here’s an example script which sets some font info values in the current font. The font info is defined in a dictionary, but it could be coming from an external file or a dialog.

```python
font = CurrentFont()

fontInfo = {
  'familyName' : 'MyFamily',
  'styleName' : 'Black Italic',
  'styleMapFamilyName' : 'Black',
  'styleMapStyleName' : 'italic',
}

for attr, value in fontInfo.items():
    setattr(font.info, attr, value)
```

> There’s also a `delattr()` function to delete an attribute from an object.
{: .note }

## Character encoding

### chr()

The `chr()` function returns a unicode character for a given unicode value (as a decimal number):

```python
>>> chr(192)
```

```console
'À'
```

### ord()

The `ord()` function does the opposite from `char()` – it returns the unicode number for a given character:

```python
>>> ord('A')
```

```console
65
```

{% comment %}
### open()
### input()
{% endcomment %}
