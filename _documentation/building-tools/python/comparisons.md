---
layout: page
title: Comparisons
level: beginner
---

* Table of Contents
{:toc}

When comparing two objects it is important to make a distinction between *value* and *identity*.

Python has comparison and identity operators to compare two objects and decide the relation between them.

## Comparing values

Comparison operators allow us to compare two objects nummerically. The result of a comparison expression is always a boolean value.

Here is an overview of all the comparisons we can make between two values:

Do the objects have the same value?

```python
>>> 12 == 20
```
```console
False
```

```python
>>> 20 == 20.0
```
```console
True
```

Do the objects have different values?

```python
>>> 12 != 20
```
```console
True
```

```python
>>> 20 != 20.0
```
```console
False
```

Is the first object bigger than the second?

```python
>>> 12 > 20
```
```console
False
```

```python
>>> 20 > 20.0
```
```console
False
```

Is the first object smaller than the second?

```python
>>> 12 < 20
```
```console
True
```

```python
>>> 20 < 20.0
```
```console
False
```

Is the first object bigger than or equal to the second?

```python
>>> 12 >= 20.0
```
```console
False
```

```python
>>> 20 >= 20.0
```
```console
True
```

Is the first object smaller than or equal to the second?

```python
>>> 12 <= 20
```
```console
True
```

```python
>>> 20 <= 20.0
```
```console
True
```

{% comment %}
add some docs about: 10 < n < 100
{% endcomment %}

## Comparing identity

Two objects can have the same *value* and still have different *identities* – they are not the same ‘thing’.

To compare identities we use the identity operator `is`:

```python
>>> 10 is 10.0
```
```console
False
```

In this example, the two objects have the same value but different identities – the first one is an `int`, and the second is a `float`.

> - [Comparisons](http://docs.python.org/3.6/library/stdtypes.html#comparisons)
{: .seealso }

## Testing ‘truthiness’

Every object in Python can be converted into a boolean. The general rule is that `0` and any empty collections are converted to `False`, and anything else is converted to `True`.

Let’s have a look at examples with different data types:

```python
>>> bool('hello') # string
```
```console
True
```

```python
>>> bool('') # empty string
```
```console
False
```

```python
>>> bool(['a', 'b', 'c']) # list
```
```console
>>> True
```

```python
>>> bool([]) # empty list
```
```console
False
```

```python
>>> bool(1.1) # float
```
```console
True
```

```python
>>> bool(0.0) # float zero
```
```console
False
```

```python
>>> bool(None)
```
```console
False
```

> - [Truth Value Testing](http://docs.python.org/3.6/library/stdtypes.html#truth-value-testing)
{: .seealso }
