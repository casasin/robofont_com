---
layout: page
title: Common Value Types
level: intermediate
tags:
    - FontParts
---

## String
{: #type-string}

Unicode (unencoded) or string. Internally everything is a unicode string.

## Integer/Float
{: #type-int-float}

Integers and floats are interchangable in FontParts (unless the specification states that only one is allowed).

## Coordinate
{: #type-coordinate}

An immutable iterable contaning two Integer/Float representing:

> `x`
> `y`

## Angle
{: #type-angle }

define the angle specifications here. Direction, degrees, etc. This will always be a float.

## Identifier
{: #type-identifier }

A String following the UFO identifier conventions.

## Color
{: #type-color }

An immutable iterable containing four Integer/Float representing:

> `red`
> `green`
> `blue`
> `alpha`

Values are from 0 to 1.0.

## Transformation Matrix
{: #type-transformation }

An immutable iterable defining a 2x2 transformation plus offset (aka Affine transform). The default is (1, 0, 0, 1, 0, 0).

## Immutable List
{: #type-tuple }

This must be an immutable, ordered iterable like a tuple.