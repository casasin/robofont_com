---
layout: page
title: FontParts + extras
tags:
  - scripting
  - fontParts
tree:
  - ../../api/fontParts/*
treeCanHide: true
level: intermediate
---

FontParts is an application-independent object model and API for programmatically creating and editing parts of fonts during the type design process.

<object data="{{ site.baseurl }}/images/building-tools/fontparts-map.svg" type="image/svg+xml"></object>

**FontParts is the most important API when working with Python in RoboFont.**

The official FontParts documentation can be found [here][FontParts Docs].

RoboFont 3 implements the FontParts API, and extends it by adding some custom methods and attributes.

> - {% internallink "toolkit/understanding-contours" %}
> - {% internallink "toolkit/pens" %}
> - {% internallink "toolkit/glyphmath" %}
> - {% internallink "toolkit/boolean-glyphmath" %}
> - {% internallink "toolkit/transformations" %}
> - {% internallink "building-tools/api/value-types" %}
{: .seealso }

> - the FontParts API was introduced in RoboFont 3; RoboFont 1 used the [RoboFab] API
> - there are some {% internallink "robofab-fontparts" text="differences between RoboFab and FontParts" %}
{: .warning }

[RoboFab]: http://robofab.org/
[FontParts Docs]: http://fontparts.readthedocs.io/
