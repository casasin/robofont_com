---
layout: page
title: Other libraries
level: intermediate
---

* Table of Contents
{:toc}

## Embedded libraries

RoboFont is built on top of several font-related Python libraries, which are embedded in RoboFont and can be used in your own scripts. Their APIs are available out-of-the-box, no additional installation is required.

### Vanilla

Vanilla is a toolkit for creating windows, dialogs and other native macOS UI elements with Python.

- [vanilla documentation (ReadTheDocs)](http://ts-vanilla.readthedocs.io/)
- [vanilla source code (GitHub)](http://github.com/typesupply/vanilla)

> - {% internallink "building-tools/toolspace/vanilla" %}
{: .seealso }

### Other embedded libraries

The following libraries are also embedded in RoboFont:

<table>
  <tr>
    <th width='30%'>library</th>
    <th width='70%'>used for…</th>
  </tr>
  <tr>
    <td><a href='http://github.com/typemytype/booleanOperations'>booleanOperations</a></td>
    <td>performing boolean operations on contours</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/compositor'>compositor</a></td>
    <td>laying out text with support for OpenType features</td>
  </tr>
  <tr>
    <td><a href='http://github.com/googlei18n/cu2qu/'>cu2qu</a></td>
    <td>converting cubic bezier curves to quadratic</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/defcon'>defcon</a></td>
    <td>providing base functionality for font objects</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/defconappkit'>defconAppKit</a></td>
    <td>creating UIs for font editing applications</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/dialogkit'>dialogKit</a></td>
    <td>creating UIs that work across different font editors</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/extractor'>extractor</a></td>
    <td>extracting data from font binaries into UFO objects</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/feapyfofum'>feaPyFoFum</a></td>
    <td>writing OpenType features dynamically</td>
  </tr>
  <!--
  <tr>
    <td><a href='#'>fontCompiler</a></td>
    <td>???</td>
  </tr>
  -->
  <tr>
    <td><a href='http://github.com/typesupply/fontmath'>fontMath</a></td>
    <td>interpolating glyphs, fonts and other kinds of font data</td>
  </tr>
  <tr>
    <td><a href='http://github.com/robofab-developers/fontpens'>fontPens</a></td>
    <td>a collection of classes implementing the pen protocol</td>
  </tr>
  <tr>
    <td><a href='http://github.com/fonttools/fonttools'>fontTools</a></td>
    <td>dealing with binaries (OpenType, TrueType etc)</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typemytype/glyphConstruction'>glyphConstruction</a></td>
    <td>building glyphs from other glyphs</td>
  </tr>
  <tr>
    <td><a href='http://github.com/LettError/glyphNameFormatter'>glyphNameFormatter</a></td>
    <td>generating glyph name lists from unicode data</td>
  </tr>
  <tr>
    <td><a href='http://github.com/LettError/MutatorMath'>mutatorMath</a></td>
    <td>interpolating in multiple dimensions</td>
  </tr>
  <tr>
    <td><a href='http://github.com/unified-font-object/ufoNormalizer'>ufoNormalizer</a></td>
    <td>normalizing XML and other data inside UFOs</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/ufo2fdk'>ufo2fdk</a></td>
    <td>generating OTFs from UFOs with the AFDKO</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/ufo2svg'>ufo2svg</a></td>
    <td>converting UFOs to SVG fonts</td>
  </tr>
  <tr>
    <td><a href='http://github.com/unified-font-object/ufoLib'>ufoLib</a></td>
    <td>reading and writing UFOs</td>
  </tr>
  <tr>
    <td><a href='http://github.com/typesupply/wofftools'>woffTools</a></td>
    <td>verifying and examining WOFF files</td>
  </tr>
</table>

> - {% internallink "technical-specification#embedded-libraries" text="Technical Specifications: Embedded libraries" %}
> - {% internallink "how-tos/overriding-embedded-libraries" %}
{: .seealso }

## Standard libraries

All standard Python modules are also available in RoboFont.

Check the [The Python Standard Library] for a complete list of modules included in Python.

[The Python Standard Library]: http://docs.python.org/3.6/library/index.html

> - {% internallink "building-tools/python/standard-modules" %}
{: .seealso }

## External libraries

In addition to embedded and standard libraries, which are available out-of-the-box in RoboFont, you can also install and use one of the thousands of third-party Python modules available.

All local packages are accessible in RoboFont, if they are properly installed in the same python version as the embedded one in RoboFont (python {{ site.data.versions.python }}) site-packages folder.

For a complete list of packages, check [PyPI - the Python Package Index].

[PyPI - the Python Package Index]: http://pypi.python.org/pypi?%3Aaction=browse

> - {% internallink "building-tools/python/external-modules" %}
> - {% internallink "building-tools/python/custom-modules" %}
{: .seealso }
