---
layout: page
title: Glyph Editor
tags:
  - contours
tree:
  - tools
  - components
  - anchors
  - guidelines
  - layers
  - images
  - display-options
  - transform
treeCanHide: true
level: beginner
---

* Table of Contents
{:toc}

The Glyph Editor is where all glyph-specific data is created and edited, one glyph at a time.

{% image workspace/glyph-editor.png %}

## Tools

The Glyph Editor offers basic interactive tools to manipulate glyph shapes:

{% image workspace/glyph-editor_tools.png %}

{% tree "/documentation/workspace/glyph-editor/tools" levels=1 %}

> More tools can be added to the Glyph Editor’s toolbar by {% internallink "documentation/extensions" %}.
{: .note }

## Glyph data

A glyph contains different kinds of visual and non-visual data.

Non-visual glyph data can be edited in the *Glyph* section of the {% internallink "inspector" text="Inspector panel" %}:

{% image workspace/inspector_glyph.png %}

Visual glyph data can be edited interactively in the Glyph View:

- {% internallink "glyph-editor/components" %}
- {% internallink "glyph-editor/anchors" %}
- {% internallink "glyph-editor/guidelines" %}
- {% internallink "glyph-editor/layers" %}
- {% internallink "glyph-editor/images" %}

## Display options

The Glyph View can display several layers of glyph data. Use the {% internallink "glyph-editor/display-options" %} menu (at the bottom left of the view) to configure the Glyph Editor to the task at hand, hiding irrelevant information and leaving only relevant layers visible.

{% image workspace/glyph-editor_display-options-buttons_.png %}

## Transform Mode

{% internallink "glyph-editor/transform" %} allows you to apply scale, rotation and skew transformations interactively to different types of objects.

{% image workspace/glyph-editor_transform.png %}

## Glyph menu

When the Glyph Editor is active, the options in the {% internallink "workspace/application-menu#glyph" text="Glyph menu" %} become available (for all tools).

{% image workspace/application-menu_glyph.png %}
