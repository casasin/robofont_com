---
layout: page
title: Font Info sheet
tags:
  - mastering
tree:
  - general
  - opentype
  - postscript
  - woff
  - misc
  - robofont
treeCanHide: true
level: beginner
---

The Font Info sheet is an interface for editing font naming fields, vertical metrics and other font meta-data. It also holds RoboFont-specific defaults that are stored in the UFO.

{% image workspace/font-info.png %}

The Font Info is organized in sections (tabs) and subsections:

{% tree "/documentation/workspace/font-info-sheet/" levels=1 %}

> - {% internallink "setting-font-infos" text="How-To: Setting Font Infos" %}
> - {% internallink "setting-font-names" text="How-To: Setting Font Names" %}
> - [fontinfo.plist (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist/)
{: .seealso }
