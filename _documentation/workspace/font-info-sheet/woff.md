---
layout: page
title: WOFF
tags:
  - WOFF
level: beginner
---

* Table of Contents
{:toc}

The WOFF settings contain information specific to WOFF fonts.

## Identification

{% image workspace/font-info_woff_identification.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Major Version</td>
    <td>Major version of the font.</td>
  </tr>
  <tr>
    <td>Minor Version</td>
    <td>Minor version of the font.</td>
  </tr>
  <tr>
    <td>Unique ID</td>
    <td>Identification string. Corresponds to the WOFF <code>uniqueid</code>. <span class="required">Required.</span></td>
  </tr>
</table>

## Vendor

{% image workspace/font-info_woff_vendor.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Name</td>
    <td>Font vendor. Corresponds to the WOFF <code>vendor</code> element <code>name</code> attribute. <span class="required">Required.</span></td>
  </tr>
  <tr>
    <td>URL</td>
    <td>Font vendor URL. Corresponds to the WOFF <code>vendor</code> element <code>url</code> attribute.</td>
  </tr>
  <tr>
    <td>Dir</td>
    <td>Writing direction. The options are <code>ltr</code> and <code>rtl</code>. Corresponds to the WOFF <code>vendor</code> element <code>dir</code> attribute.</td>
  </tr>
  <tr>
    <td>Class</td>
    <td>Class tokens. Corresponds to the WOFF <code>vendor</code> element <code>class</code> attribute.</td>
  </tr>
</table>

## Credits

{% image workspace/font-info_woff_credits.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>  </tr>
  <tr>
    <td>Name</td>
    <td>The name for the credit. Corresponds to the WOFF <code>credit</code> element <code>name</code> attribute. <span class="required">Required.</span></td>
  </tr>
  <tr>
    <td>Role</td>
    <td>The role for the credit. Corresponds to the WOFF <code>credit</code> element <code>role</code> attribute.</td>
  </tr>
  <tr>
    <td>URL</td>
    <td>The url for the credit. Corresponds to the WOFF <code>credit</code> element <code>url</code> attribute.</td>
  </tr>
  <tr>
    <td>Dir</td>
    <td>Writing direction. The options are <code>ltr</code> and <code>rtl</code>. Corresponds to the WOFF <code>credit</code> element <code>dir</code> attribute.</td>
  </tr>
  <tr>
    <td>Class</td>
    <td>Class tokens. Corresponds to the WOFF <code>credit</code> element <code>class</code> attribute.</td>
  </tr>
</table>

## Description

{% image workspace/font-info_woff_description.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>URL</td>
    <td>A URL for the description. Corresponds to the WOFF <code>description</code> element <code>url</code> attribute.</td>
  </tr>
  <tr>
    <td>Text</td>
    <td>A list of WOFF Metadata Text Records for the description. This list must contain at least one text record.</td>
  </tr>
  <tr>
    <td>Language</td>
    <td>Language. Corresponds to the WOFF <code>text</code> element <code>xml:lang</code> attribute.</td>
  </tr>
  <tr>
    <td>Dir</td>
    <td>Writing direction. The options are <em>ltr</em> and <em>rtl</em>. Corresponds to the WOFF <code>text</code> element <code>dir</code> attribute.</td>
  </tr>
  <tr>
    <td>Class</td>
    <td>Class tokens. Corresponds to the WOFF <code>text</code> element <code>class</code> attribute.</td>
  </tr>
</table>

## Legal

{% image workspace/font-info_woff_legal.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Copyright Text</td>
    <td>A list of WOFF Metadata Text Records for the copyright. This list must contain at least one text record.</td>
  </tr>
  <tr>
    <td>Trademark Text</td>
    <td>A list of WOFF Metadata Text Records for the trademark. This list must contain at least one text record.</td>
  </tr>
  <tr>
    <td>License URL</td>
    <td>A URL for the license. Corresponds to the WOFF <code>license</code> element <code>url</code> attribute.</td>
  </tr>
  <tr>
    <td>License ID</td>
    <td>Ad ID for the license. Corresponds to the WOFF <code>license</code> element <code>id</code> attribute.</td>
  </tr>
  <tr>
    <td>License Text</td>
    <td>A list of WOFF Metadata Text Records for the description. This list must contain at least one text record.</td>
  </tr>
  <tr>
    <td>Licensee Name</td>
    <td>The licensee. Corresponds to the WOFF <code>licensee</code> element <code>name</code> attribute. <span class="required">Required.</span></td>
  </tr>
  <tr>
    <td>Licensee Dir</td>
    <td>Writing direction. The options are <code>ltr</code> and <code>rtl</code>. Corresponds to the WOFF <code>vendor</code> element <code>dir</code> attribute.</td>
  </tr>
  <tr>
    <td>Licensee Class</td>
    <td>Class tokens. Corresponds to the WOFF <code>licensee</code> element <code>class</code> attribute.</td>
  </tr>
</table>

> - [WOFF Data (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/fontinfo.plist/#woff-data)
> - [WOFF specification](http://www.w3.org/TR/WOFF/)
{: .seealso }
