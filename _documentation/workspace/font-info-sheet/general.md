---
layout: page
title: General font info
tags:
level: beginner
---

* Table of Contents
{:toc}

The General settings contain basic information about the font, and are used to set more specific values in other parts of the Font Info.

## Identification

The Identification section contains the name and version of the font.

{% image workspace/font-info_general_identification.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Family Name</td>
    <td>The font’s family name. <span class="required">Required</span></td>
  </tr>
  <tr>
    <td>Style Name</td>
    <td>The font’s style name. <span class="required">Required</span></td>
  </tr>
  <tr>
    <td>Style Map Family Name</td>
    <td>Family name used for bold, italic and bold-italic style mapping. <span class="recommended">Recommended</span></td>
  </tr>
  <tr>
    <td>Style Map Style</td>
    <td>The style map style. The possible values are <code>regular</code>, <code>italic</code>, <code>bold</code> and <code>bold italic</code>.</td>
  </tr>
  <tr>
    <td>Version Major</td>
    <td>Major version. <span class="recommended">Recommended</span></td>
  </tr>
  <tr>
    <td>Version Minor</td>
    <td>Minor version. <span class="recommended">Recommended</span></td>
  </tr>
</table>

## Dimensions

The Dimensions section contains the vertical metrics and slant angle of the font.

{% image workspace/font-info_general_dimensions.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Units Per Em</td>
    <td>Units per em. <span class="required">Required</span></td>
  </tr>
  <tr>
    <td>Descender</td>
    <td>Descender value. Must be <code>0</code> or a negative number. <span class="required">Required</span></td>
  </tr>
  <tr>
    <td>x-height</td>
    <td>x-height value. <span class="required">Required</span></td>
  </tr>
  <tr>
    <td>Cap-height</td>
    <td>Cap height value. <span class="required">Required</span></td>
  </tr>
  <tr>
    <td>Ascender</td>
    <td>Ascender value. <span class="required">Required</span></td>
  </tr>
  <tr>
    <td>Italic Angle</td>
    <td>Italic angle.</td>
  </tr>
</table>

## Legal

{% image workspace/font-info_general_legal.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Copyright</td>
    <td>Copyright statement. Corresponds to the OpenType <code>name</code> table name ID 0. <span class="recommended">Recommended</span></td>
  </tr>
  <tr>
    <td>Trademark</td>
    <td>Trademark statement. Corresponds to the OpenType <code>name</code> table name ID 7. <span class="recommended">Recommended</span></td>
  </tr>
  <tr>
    <td>License</td>
    <td>License text. Corresponds to the OpenType <code>name</code> table name ID 13. <span class="recommended">Recommended</span></td>
  </tr>
  <tr>
    <td>License URL</td>
    <td>URL for the license. Corresponds to the OpenType <code>name</code> table name ID 14. <span class="recommended">Recommended</span></td>
  </tr>
</table>

## Parties

{% image workspace/font-info_general_parties.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Designer</td>
    <td>Designer name. Corresponds to the OpenType <code>name</code> table name ID 9. <span class="recommended">Recommended</span></td>
  </tr>
  <tr>
    <td>Designer URL</td>
    <td>URL for the designer. Corresponds to the OpenType <code>name</code> table name ID 12. <span class="recommended">Recommended</span></td>
  </tr>
  <tr>
    <td>Manufacturer</td>
    <td>Manufacturer name. Corresponds to the OpenType <code>name</code> table name ID 8. <span class="recommended">Recommended</span></td>
  </tr>
  <tr>
    <td>Manufacturer URL</td>
    <td>Manufacturer URL. Corresponds to the OpenType <code>name</code> table name ID 11. <span class="recommended">Recommended</span></td>
  </tr>
</table>

## Note

{% image workspace/font-info_general_note.png %}

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Note</td>
    <td>Arbitrary note about the font. <strong>Not included in generated fonts.</strong> Use it to keep notes about the UFO (changes, thoughts, to-do list, etc).</td>
  </tr>
</table>
