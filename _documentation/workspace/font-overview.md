---
layout: page
title: Font Overview
tags:
  - glyph set
level: beginner
---

* Table of Contents
{:toc}

The Font Overview displays an overview of all glyphs in the font, and offers several tools to create and manage glyph sets, order glyphs, find glyphs, sort glyphs by different attributes, etc.

{% image workspace/font-overview.png %}

> - {% internallink "how-tos/adding-and-removing-glyphs" %}
> - {% internallink "how-tos/renaming-glyphs" %}
> - {% internallink "how-tos/marking-glyphs" %}
> - {% internallink "how-tos/sorting-glyphs" %}
> - {% internallink "how-tos/searching-glyphs" %}
> - {% internallink "how-tos/using-smart-sets" %}
{: .seealso }

## Display Modes

The Font Overview has two display modes, Tiled Glyphs (Glyph Cells) and Glyph List. Click on the icons at the bottom left of the window to switch between the two.

{% image workspace/font-overview_display-modes.png %}

### Tiles Mode

The Tiles Mode shows the font as a grid of glyph cells. The size of the cells can be adjusted by zooming in / out with the keyboard (⌘+ / ⌘-) or with the slider at the bottom.

{% image workspace/font-overview_glyph-cells.png %}

- Large cells show several layers of data: glyph name, glyph shape, width and margins, layers, vertical metrics. Smaller cells show only the glyph shape.

- Unsaved glyphs have their names highlighted until the font is saved again.

- A glyph’s mark color is used as the background color of the cell.

- A small `L` indicates that the glyph contains more than one layer.

- A small `N` indicates that the glyph contains a note.

### List Mode

The List Mode displays the font as one big table, showing several types of data for each glyph.

{% image workspace/font-overview_list-mode.png %}

Click on the column headers to sort the glyphs according to different attributes.

Individual columns can be shown/hidden using the table’s contextual menu (right-click the table header to open it):

{% image workspace/font-overview_list-mode-columns.png %}

## Actions

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>single click</td>
    <td>Select single glyph.</td>
  </tr>
  <tr>
    <td>⌥ + single click (Multi‑Window mode)</td>
    <td>Opens a new Glyph Window.</td>
  </tr>
  <tr>
    <td>single click (Single Window mode)</td>
    <td>Select single glyph and send it to the Glyph View.</td>
  </tr>
  <tr>
    <td>double click (Single Window mode)</td>
    <td>Send the glyph to the Glyph View.</td>
  </tr>
  <tr>
    <td>click + drag</td>
    <td>
      <b>Drag the selection on the same Font Overview:</b>
      <ul>
        <li>Drop between the glyph cells: Reorder the glyphs in the font.</li>
        <li>Drop on a glyph cell: Copy the dragged glyph into the destination glyph cell. Use ⌥ to copy as component.</li>
      </ul>
      <b>Drag the selection to other views:</b>
      <ul>
        <li>Other Font Overview: Add glyphs to the other font, and update the glyph order based on the drop position.</li>
        <li>Space Center: Set the glyph names as input for spacing. Use ⌥ to append the glyph names to the current input text.</li>
        <li>Smart Sets: Creates a new Smart Set with the dragged glyphs.</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>⌘ + click + drag</td>
    <td>Modify the selection while moving the mouse.</td>
  </tr>
  <tr>
    <td>copy</td>
    <td>Copies the selected glyphs to the clipboard as glyph objects, and the glyph names as string representation.</td>
  </tr>
  <tr>
    <td>copy as component</td>
    <td>Copies the selected glyph as component (single glyph selection only).</td>
  </tr>
  <tr>
    <td>paste</td>
    <td>
      <ul>
        <li>If there is no selection, the copied glyphs are appended to the font.</li>
        <li>If the amount of selected glyphs is the same as the amount of copied glyphs, RoboFont will replace the selected glyphs with the copied glyphs.</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>undo/redo</td>
    <td>Undo or redo the selected glyph(s).</td>
  </tr>
  <tr>
    <td>start typing a glyph name</td>
    <td>Tries to find and select that glyph.</td>
  </tr>
  <tr>
    <td>⌘ + J</td>
    <td>Opens a Jump To Glyph window.</td>
  </tr>
  <tr>
    <td>delete</td>
    <td>Remove the glyph from the font.</td>
  </tr>
  <tr>
    <td>⌥ + delete</td>
    <td>Remove the glyph from the font and the template glyph from the Font Overview.</td>
  </tr>
</table>

> - {% internallink 'how-tos/working-with-large-fonts' %}
{: .seealso }

{% comment %}

## Load All Glyphs

If a font contains too many glyphs, a Load All Glyphs button will appear at the bottom left of the Font Overview window. Pressing this button forces RoboFont to load data from all glyphs in the current font.

{% image workspace/font-overview_load-all-glyphs.png %}

> RoboFont has a preference named `loadFontGlyphLimit`, which makes loading large fonts faster by controlling how many glyphs are loaded when an UFO is opened.
>
> If a font has more glyphs than `loadFontGlyphLimit`, the additional glyphs which are needed to render the Font Cell View or the Current Glyph View are loaded only as required.
>
> Sometimes, however, it might be necessary to load data from all glyphs – for example, to build a complete unicode map of the font. Since the unicodes are stored in each glyph, all `.glif` files in the font need to be parsed, and that takes a while for very large fonts.
{: .note }

{% endcomment %}
