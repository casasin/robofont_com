---
layout: page
title: Preferences Window
tags:
  - preferences
tree:
  - glyph-view
  - character-set
  - space-center
  - sort
  - menus
  - python
  - extensions
  - miscellaneous
treeCanHide: true
level: beginner
---

* Table of Contents
{:toc}

The Preferences Window is a place where all user defaults can be edited.

{% image workspace/preferences.png %}

## Preference Groups

Preferences are grouped into sections and subsections:

{% tree page.url levels=1 %}

## Applying changes

For changes to the Preferences to have an effect, click on the ‘Apply’ button at the top right of the Toolbar.

{% image workspace/preferences_apply.png %}

Some changes may require a restart of RoboFont. If that’s the case, a note will be displayed at the bottom of the Prefefences Window.

{% image workspace/preferences_restart.png %}

## Options

{% image workspace/preferences_reset.png %}

<table>
  <tr>
    <th width='30%'>option</th>
    <th width='70%'>description</th>
  </tr>
  <tr>
    <td>Reset All</td>
    <td>Restore all Preferences to their default values.</td>
  </tr>
  <!-- this option is not available in RF3?
  <tr>
    <td>Reset “Don’t Show This” Warnings</td>
    <td></td>
  </tr>
  -->
  <tr>
    <td>Export</td>
    <td>Export all Preferences to a <code>.roboFontSettings</code> file.</td>
  </tr>
  <tr>
    <td>Import</td>
    <td>Import Preferences from a <code>.roboFontSettings</code> file.</td>
  </tr>
</table>

> - {% internallink "workspace/preferences-editor" %}
{: .seealso }
