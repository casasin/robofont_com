---
layout: page
title: Components
tags:
level: beginner
---

* Table of Contents
{:toc}

Components are part of the outline descriptions of a glyph. They are different from regular contours because they are *references* to other glyphs in the font.

{% image workspace/components.png caption="contour (slash) and component (o)" %}

Components make it possible to create glyphs out of other glyphs without copy-pasting data. With components, changing the base glyph also means changing all references to it in the font.

- The base glyph referenced by a component may contain components.
- The base glyph must not create a circular reference to the glyph that contains the component.
- Components must only reference glyphs within the same layer that the component belongs to.

> - [Component (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/glyphs/glif/#component)
{: .seealso }

## Adding Components

To add components to a glyph, follow these steps in the Glyph Editor:

{% image workspace/components_add.png %}

1. Right-click on the canvas to activate the Editing Tool’s {% internallink "glyph-editor/tools/editing-tool#contextual-menus" text="contextual menu" %}.
2. Choose *Add Component* to open the Add Components sheet.
3. Use the options (glyph name, glyphs list) to choose a base glyph.
4. Click on the *Add Component* button to insert the selected glyph into the current glyph as a component.

### Actions

<table>
  <tr>
    <th width='35%'>action</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>typing…</td>
    <td>Restrict list to glyph names starting with typed characters.</td>
  </tr>
  <tr>
    <td>arrow keys (↑ and ↓)</td>
    <td>Move up or down in the selection.</td>
  </tr>
  <tr>
    <td>tab</td>
    <td>Jumps from Starts With to Contains and back.</td>
  </tr>
  <tr>
    <td>enter</td>
    <td>Add selected glyph as component.</td>
  </tr>
  <tr>
    <td>Copy Metrics</td>
    <td>Copy the component’s metrics to the current glyph.</td>
  </tr>
</table>

## Components Inspector

The properties of a Component object (base glyph, transformation) can be edited using the {% internallink "inspector#components" text="Components" %} section of the Inspector.

{% image workspace/inspector_components.png %}

## Contextual menu

The glyph’s {% internallink "glyph-editor/tools/editing-tool#right-click-on-glyph-with-components" text="contextual menu" %} (activated with a right-click) shows a list of all components in the glyph, and makes it possible to switch to the component’s base glyph for editing.

{% image workspace/glyph-editor_editing-tool_contextual-menu_component.png %}

## Working with components

Components are useful when working with modular shapes, or when developing fonts with multingual character sets.

Components can be freely {% internallink "glyph-editor/transform" text="transformed" %} (translated, scaled, rotated and skewed).

If you need to edit a component’s contours, you have 2 options:

1. Go to its base glyph and edit the contours.

    *These changes apply to all ocurrences of this glyph as component in the font.*

2. Decompose the component (using Decompose from the contextual menu). This will convert the base glyph’s outlines into regular contours, and remove any reference to the base glyph. Edit the contours as you like.

    *There is no more component, so these changes apply only to the currently glyph.*

## Accented glyphs

The main use case for components is building accented glyphs. For example, an accented character `/aacute` is typically built with components to `/a` (base) and `/acute` (accent).

> - {% internallink "how-tos/building-accented-glyphs" %}
{: .seealso }
