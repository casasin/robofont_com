---
layout: page
title: Layers
tags:
  - layers
level: beginner
---

* Table of Contents
{:toc}

RoboFont supports multiple layers in a single font.

The layering system in RoboFont is very flexible, and can be used in many ways:

- to separate different stages of a project
- to facilitate collaboration with other designers
- to store multiple masters of a typeface
- to store different layers of a color font
- *anything else you can think of*

> RoboFont 3 implements support for layers via UFO3, while RoboFont 1 implements layers as custom extensions to UFO2.
>
> Layers are more flexible in RF3: different layers of a glyph can have different widths. In RF1, all layers of a glyph must have the same width.
{: .note }

## Creating and editing layers

Use the *Layers* section of the {% internallink "inspector#layers" text="Inspector panel" %} to add, delete, edit and reorder layers.

{% image workspace/layers_inspector.png %}

Layer colors
: Each layer has a color. Colors are used to differentiate layers when they are in the background of the Glyph View.

Default layer
: The star indicates the default layer. To make another layer the default, right-click it to open a contextual menu, and choose *Make Default Layer*.

> - Layer names are restricted to ASCII characters. Spaces, slashes and semicolons are not allowed.
{: .note }

### Layer options

{% image workspace/layers_inspector_icons.png %}

<table>
  <thead>
    <tr>
      <th width="20%">button</th>
      <th width="80%">action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><img src="../../../../images/workspace/inspector_layers_plus.png" /></td>
      <td>Add a layer.</td>
    </tr>
    <tr>
      <td><img src="../../../../images/workspace/inspector_layers_minus.png" /></td>
      <td>Remove selected layer.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td><img src="../../../../images/workspace/inspector_layers_eye.png" /></td>
      <td>Toggle visibility of selected layer.</td>
    </tr>
    <tr>
      <td><img src="../../../../images/workspace/inspector_layers_fill.png" /></td>
      <td>Toggle fill color in selected layer.</td>
    </tr>
    <tr>
      <td><img src="../../../../images/workspace/inspector_layers_stroke.png" /></td>
      <td>Toggle stroke color in selected layer.</td>
    </tr>
    <tr>
      <td><img src="../../../../images/workspace/inspector_layers_point.png" /></td>
      <td>Toggle points in selected layer.</td>
    </tr>
    <tr>
      <td><img src="../../../../images/workspace/inspector_layers_coordinate.png" /></td>
      <td>Toggle point coordinates in selected layer.</td>
    </tr>
  </tbody>
</table>

## Copying between layers

> [RoboFont 3.2]({{site.baseurl}}/downloads/)
{: .version-badge }


A *Jump To Layer* pop-up window is also avaialable to quickly copy between layers of a the same glyph. It can be opened by pressing `L` while working in the {% internallink 'workspace/glyph-editor' %}.

{% image workspace/glyph-editor_jump-to-layer.png %}

<table>
  <thead>
    <tr>
      <th width="25%">option</th>
      <th width="75%">action</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Copy</td>
      <td>Copy the contents of the current layer to one or more layers.</td>
    </tr>
    <tr>
      <td>Move</td>
      <td>Copy the contents of the current layer to one or more layers, and clear the contents of the current layer.</td>
    </tr>
    <tr>
      <td>Flip</td>
      <td>Switch the contents of the current layer with the contents of another layer, and vice-versa.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Copy Metrics</td>
      <td>Copy glyph width when copying between layers.</td>
    </tr>
  </tbody>
</table>

