---
layout: page
title: Measurement Tool
tags:
  - contours
level: beginner
---

{% image workspace/glyph-editor_measurement-tool.png %}

A tool to draw interactive measurement lines over a glyph.

{% image workspace/glyph-editor_measurement-line.png %}

## Actions

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Define the starting point of a measurement line.</td>
  </tr>
  <tr>
    <td>⌥ + click</td>
    <td>Enable multiple measurement lines.</td>
  </tr>
  <tr>
    <td>click (no drag)</td>
    <td>Remove all measurement lines.</td>
  </tr>
  <tr>
    <td>drag</td>
    <td>Set the end point of a measurement line.</td>
  </tr>
  <tr>
    <td>⇧ + drag</td>
    <td>Constrain the measurement line to x, y or 45° axes. When started on a segment, it will maintain the angle from the point on that segment.</td>
  </tr>
  <tr>
    <td>click + drag start/end point</td>
    <td>Modify the measurement line.</td>
  </tr>
  {% comment %}
  <!-- can't find this option in the Application Menu (??) -->
  <tr>
    <th colspan="2">Menu > Glyph</th>
  </tr>
  <tr>
    <td>Show Measurement Info</td>
    <td>Show/hide measurement info in the Glyph View.</td>
  </tr>
  {% endcomment %}
</table>

## Measurement Tool in action

{% include embed vimeo=47779626 %}
