---
layout: page
title: Slice Tool
tags:
  - contours
level: beginner
---

{% image workspace/glyph-editor_slice-tool.png %}

A tool to add points to contours at the intersections with a dragged line.

{% image workspace/slice-line.png %}

When the slice line has 2 intersection with the same contour, it will cut the contour into separate shapes. Otherwise, it will just add points.

## Actions

<table>
  <tr>
    <th width="35%">action</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>click</td>
    <td>Define the starting point of a slicing line.</td>
  </tr>
  <tr>
    <td>drag</td>
    <td>Define the end point of a slicing line.</td>
  </tr>
  <tr>
    <td>⇧ + drag</td>
    <td>Constrain the slicing line to x, y or 45° axes.</td>
  </tr>
  <tr>
    <td>mouse up</td>
    <td>Slice the glyph with the slicing line.</td>
  </tr>
</table>

> Guidelines can also be used to slice contours. See {% internallink "glyph-editor/guidelines" %}.
{: .seealso }
