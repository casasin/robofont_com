---
layout: page
title: Anchors
tags:
  - anchors
level: beginner
---

Anchors are single points with a position and a name.

{% image workspace/anchors.png %}

Anchors are used mostly as position references when building new glyphs with components. They enable a system for aligning shapes in relation to each other.

## Adding anchors

To add anchors to a glyph, follow these steps in the Glyph Editor:

{% image workspace/anchors_add.png %}

1. Right-click on the canvas to activate the Editing Tool’s {% internallink "glyph-editor/tools/editing-tool#contextual-menus" text="contextual menu" %}.
2. Choose *Add Anchor* to open the Add Anchor sheet.
3. Give the anchor a name (required).
4. Click on the *Add* button or press Enter to create the anchor.

> - [Anchor (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/glyphs/glif/#anchor)
> - {% internallink "how-tos/building-accented-glyphs" %}
{: .seealso }
