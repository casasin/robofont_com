---
layout: page
title: Scripting Window
tags:
  - scripting
level: beginner
---

* Table of Contents
{:toc}

The Scripting Window is a simple code editor for writing and running Python scripts.

{% image workspace/scripting-window.png %}

The code editor supports syntax highlighting for Python. The colors can be configured in the {% internallink "preferences-window/python" %}.

> - {% internallink 'scripting-environment' %}
{: .seealso }

## Options

An options menu is available by clicking on the gears icon at the bottom left:

{% image workspace/scripting-window_options.png %}

<table>
  <thead>
    <tr>
      <th width="30%">title</th>
      <th width="70%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Show line numbers</td>
      <td>Show line number before each line.</td>
    </tr>
    <tr>
      <td>Indent using spaces</td>
      <td>Indent code using spaces instead of tabs.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Window float on top</td>
      <td>Make the Scripting Window float on top of other windows.</td>
    </tr>
  </tbody>
</table>

## Toolbar

The toolbar gives access to the Scripting Window’s main functions:

<table>
  <thead>
    <tr>
      <th width="30%">title</th>
      <th width="70%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Run</td>
      <td>Execute the current script.</td>
    </tr>
    <tr>
      <td>Comment</td>
      <td>Comment the selected line(s).</td>
    </tr>
    <tr>
      <td>Uncomment</td>
      <td>Uncomment the selected line(s).</td>
    </tr>
    <tr>
      <td>Indent</td>
      <td>Add indentation to selected line(s).</td>
    </tr>
    <tr>
      <td>Dedent</td>
      <td>Remove indentation from selected line(s).</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Save</td>
      <td>Save the current script as a <code>.py</code> file.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Reload</td>
      <td>Reload the script from disk. Useful if the script has been edited by another application.</td>
    </tr>
    <tr>
      <td>New</td>
      <td>Create a new empty script.</td>
    </tr>
    <tr>
      <td>Open</td>
      <td>Open an existing script from <code>.py</code> file.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Edit With…</td>
      <td>Edit script with another application.</td>
    </tr>
  </tbody>
</table>

## Script Browser

The Script Browser is a collapsible side panel which makes it possible to navigate through all scripts in a chosen folder.

{% image workspace/scripting-window_script-browser.png %}

open a script in the code editor
: double-click a script

change the scripts folder
: click on the pop up button and choose a folder

show/hide the script browser
: click on the panel icon at the bottom left

## Code interaction

The code editor offers special ways to interact with some types of values in your code. When selected, these values can be modified interactively by using modifier keys together with the mouse or the arrow keys.

### bool

Boolean values can be toggled on/off like a switch with the mouse, or with the keyboard using the arrow keys.

<table>
  <tr>
    <th width="30%">modifiers</th>
    <th width="70%">↑ or → or ↓ or ←</th>
  </tr>
  <tr>
    <td>⌘</td>
    <td>Toggle value on/off.</td>
  </tr>
</table>

### int / float

The value of integers and floats can be modified dynamically with the mouse like sliders, or with the keyboard using the arrow keys.

<table>
  <tr>
    <th width="30%">modifiers</th>
    <th width="35%">↑ or →</th>
    <th width="35%">↓ or ←</th>
  </tr>
  <tr>
    <td><em>⌘</em></td>
    <td><code>+1</code></td>
    <td><code>-1</code></td>
  </tr>
  <tr>
    <td>⌘ + ⌥</td>
    <td><code>+0.1</code></td>
    <td><code>-0.1</code></td>
  </tr>
  <tr>
    <td>⌘ + ⇧</td>
    <td><code>+10</code></td>
    <td><code>-10</code></td>
  </tr>
  <tr>
    <td>⌘ + ⌥ + ⇧</td>
    <td><code>+0.01</code></td>
    <td><code>-0.01</code></td>
  </tr>
</table>

### tuple

Selected pairs of numbers can be modified together with the mouse or with the keyboard, like a movement in xy space.

<table>
  <tr>
    <th width="30%">modifiers</th>
    <th width="17.5%">→</th>
    <th width="17.5%">←</th>
    <th width="17.5%">↑</th>
    <th width="17.5%">↓</th>
  </tr>
  <tr>
    <td><em>⌘</em></td>
    <td><code>+1</code> x</td>
    <td><code>-1</code> x</td>
    <td><code>+1</code> y</td>
    <td><code>-1</code> y</td>
  </tr>
  <tr>
    <td>⌘ + ⌥</td>
    <td><code>+0.1</code> x</td>
    <td><code>-0.1</code> x</td>
    <td><code>+0.1</code> y</td>
    <td><code>-0.1</code> y</td>
  </tr>
  <tr>
    <td>⌘ + ⇧</td>
    <td><code>+10</code> x</td>
    <td><code>-10</code> x</td>
    <td><code>+10</code> y</td>
    <td><code>-10</code> y</td>
  </tr>
  <tr>
    <td>⌘ + ⌥ + ⇧</td>
    <td><code>+0.01</code> x</td>
    <td><code>-0.01</code> x</td>
    <td><code>+0.01</code> y</td>
    <td><code>-0.01</code> y</td>
  </tr>
</table>
