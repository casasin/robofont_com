---
layout: page
title: Python Preferences
treeTitle: Python
level: beginner
tags:
  - scripting
---

* Table of Contents
{:toc}

The Python section of the Preferences contains settings related to the {% internallink "scripting-window" %} and the {% internallink "output-window" %}.

{% image workspace/preferences_python.png %}

## Syntax coloring

The main table allows the user to customize the code editor’s coloring scheme. Each entry in the table stands for a different type of term in the Python language. To change a color, double-click its cell and use the Color Well to edit the color.

## Options

<table>
  <tr>
    <th width='35%'>option</th>
    <th width='65%'>description</th>
  </tr>
  <tr>
    <td>Font</td>
    <td>The font used in the <a href="../../scripting-window">Scripting Window</a>, <a href="../../output-window">Output Window</a> and <a href="../../features-editor">Features Editor</a>.</td>
  </tr>
  <tr>
    <td>Background</td>
    <td>Background color of the Code Editor.</td>
  </tr>
  <tr>
    <td>Text selection</td>
    <td>Text selection color in the Code Editor.</td>
  </tr>
  <tr>
    <td>Clear text output before running script</td>
    <td>Removes the output in a scripting window of a previous script.</td>
  </tr>
  <tr>
    <td>Use Future Division</td>
    <td>Use relaxed float division. Example: 10 / 3 = 3.3333. Otherwise, the result is clipped to integer: 10 / 3 = 3.<br/>See <a href="../../../building-tools/python/numbers-arithmetics#division">Numbers and arithmetics: Division</a>.</td>
  </tr>
  <tr>
    <td>Live update output</td>
    <td>Update Scripting Window output during script execution.</td>
  </tr>
  <tr>
    <td>Debugger Background</td>
    <td>Background color of the <a href="../../output-window">Output Window</a>.</td>
  </tr>
  <tr>
    <td>Debugger text</td>
    <td>Text color in the <a href="../../output-window">Output Window</a>.</td>
  </tr>
  <tr>
    <td>Pop up Output window</td>
    <td>Open the Output Window whenever something is printed, or when a warning is raised.</td>
  </tr>
  <tr>
    <td>Script folder</td>
    <td>The default folder for Python scripts. The Scripts menu shows a list of all <code>.py</code> files contained in this folder.</td>
  </tr>
</table>
