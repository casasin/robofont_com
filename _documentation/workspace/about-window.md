---
layout: page
title: About Window
level: beginner
---

The About window displays copyright and version information of your current RoboFont app.

{% image workspace/about-window.png %}

The displayed information includes:

- general credits and copyright
- RoboFont version number and build number
- embedded AFDKO version number
- embedded GlyphNameFormatter version number

This information is important when reporting bugs – always include the RoboFont and Mac OS X version numbers in your report.

<!--
{% comment %}
{% glossary AFDKO %}
{% glossary GlyphNameFormatter %}
{% endcomment %}
-->
