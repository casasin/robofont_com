---
layout: page
title: Space Center
tags:
  - spacing
level: beginner
---

* Table of Contents
{:toc}

The Space Center is a specialized window for previewing text and adjusting glyph metrics.

Type a string of characters or glyph names in the input field – a text sample in the current font is updated in real-time with each new keystroke.

{% image workspace/space-center.png %}

At the bottom of the Space Center is the Space Matrix, a table showing the horizontal metrics values for each glyph (advance width, left margin and right margin). These values can be modified to define the spacing of each glyph.

Undo/redo is supported for the selected glyph.

## Text Input Field

The input text (main input field at the top of the window) supports input in different formats:

<table>
  <tr>
    <th width='35%'>keys</th>
    <th width='65%'>action</th>
  </tr>
  <tr>
    <td>any text (<code>abc</code>)</td>
    <td>tries to match glyph names (<code>a</code>, <code>b</code>, <code>c</code>)</td>
  </tr>
  <tr>
    <td>unicode character (<code>&é</code>)</td>
    <td>tries to match the unicode values (<code>ampersand</code>, <code>eacute</code>)</td>
  </tr>
  <tr>
    <td><code>/glyphName</code></td>
    <td>explicit name the glyphName</td>
  </tr>
  <tr>
    <td><code>/glyphName@layerName</code></td>
    <td>explicit name the glyphName from an explicit layer</td>
  </tr>
  <tr>
    <td><code>/?</code></td>
    <td>display the current active glyph</td>
  </tr>
  <tr>
    <td><code>/!</code></td>
    <td>display the current selection from a font window</td>
  </tr>
  <tr>
    <td><code>\n</code></td>
    <td>new line</td>
  </tr>
</table>

Custom input strings can be stored in the {% internallink "preferences-window/space-center" %}.

## Control glyphs

The two smaller input fields around the main input field are used to define left and right control glyphs for the sample. These glyphs are shown before/after each glyph defined by the main string.

## Editing metrics

Metrics can be edited by selecting and dragging a glyph, or by using arrow keys to control the Space Matrix.

### Editing with the mouse

<table>
  <tr>
    <th width='20%'>modifiers</th>
    <th width='40%'>action</th>
    <th width='40%'>result</th>
  </tr>
  <tr>
    <td></td>
    <td>click on a glyph</td>
    <td>Select the glyph.</td>
  </tr>
  <tr>
    <td></td>
    <td>click outside of a glyph</td>
    <td>Deselect the glyph.</td>
  </tr>
  <tr>
    <td></td>
    <td>click + drag left or right margin</td>
    <td>Move the left or right margin.</td>
  </tr>
  <tr>
    <td></td>
    <td>click + drag contours</td>
    <td>Move the glyph horizontally.</td>
  </tr>
  <tr>
    <td>⌥</td>
    <td>click + drag contours</td>
    <td>Move the glyph vertically.</td>
  </tr>
  <tr>
    <td>⌥ + ⌘</td>
    <td>click + drag contours</td>
    <td>Move the glyph freely.</td>
  </tr>
</table>

### Editing with the arrow keys

If a glyph is selected, its margins can be modified using the arrow keys.

<table>
  <tr>
    <th width='20%'>modifiers</th>
    <th width='15%'>↑</th>
    <th width='15%'>↓</th>
    <th width='50%'>affects</th>
  </tr>
  <tr>
    <td></td>
    <td>-1</td>
    <td>+1</td>
    <td>width (right margin)</td>
  </tr>
  <tr>
    <td>⇧</td>
    <td>-10</td>
    <td>+10</td>
    <td>width (right margin)</td>
  </tr>
  <tr>
    <td>⇧ + ⌘</td>
    <td>-100</td>
    <td>+100</td>
    <td>width (right margin)</td>
  </tr>
  <tr>
    <td>⌥</td>
    <td>-1</td>
    <td>+1</td>
    <td>left margin</td>
  </tr>
  <tr>
    <td>⌥ + ⇧</td>
    <td>-10</td>
    <td>+10</td>
    <td>left margin</td>
  </tr>
  <tr>
    <td>⌥ + ⇧ + ⌘</td>
    <td>-100 </td>
    <td>+100 </td>
    <td>left margin</td>
  </tr>
</table>

### Nummerical fields

Input fields for Width, Left and Right take in a number, or can be defined as the result of a calculation with values of other glyphs.

<table>
  <tr>
    <th width='30%'>description</th>
    <th width='20%'>example</th>
    <th width='50%'>result</th>
  </tr>
  <tr>
    <td>any number</td>
    <td><code>123</code></td>
    <td>Sets value to the given number.</td>
  </tr>
  <tr>
    <td>equal sign followed by glyph name</td>
    <td><code>=a</code></td>
    <td>Sets value to be equal to the value of the assigned glyph.</td>
  </tr>
  <tr>
    <td>simple math expression</td>
    <td><code>=e+10-z*10</code><!--  or <code>-=10+a</code> --></td>
    <td>Sets value to be equal to the result of the evaluated expression.</td>
  </tr>
</table>

Values in nummerical fields can also be modified incrementally using the arrow keys:

<table>
  <tr>
    <th width='20%'>modifiers</th>
    <th width='15%'>↑</th>
    <th width='15%'>↓</th>
    <th width='50%'>affects</th>
  </tr>
  <tr>
    <td></td>
    <td>-1</td>
    <td>+1</td>
    <td>selected value</td>
  </tr>
  <tr>
    <td>⇧</td>
    <td>-10</td>
    <td>+10</td>
    <td>selected value</td>
  </tr>
  <tr>
    <td>⇧ + ⌘ </td>
    <td>-100</td>
    <td>+100</td>
    <td>selected value</td>
  </tr>
</table>

Arrows and tab keys can be used to move to other values using only the keyboard:

<table>
  <tr>
    <th width='20%'>modifiers</th>
    <th width='30%'>keys</th>
    <th width='50%'>description</th>
  </tr>
  <tr>
    <td>⌥</td>
    <td>↑ or → or ↓ or ←</td>
    <td>Jump to adjacent input fields.</td>
  </tr>
  <tr>
    <td></td>
    <td>tab</td>
    <td>Jump to the next input field.</td>
  </tr>
  <tr>
    <td>⇧</td>
    <td>tab</td>
    <td>Jump to the previous input field.</td>
  </tr>
  <tr>
    <td>⌥</td>
    <td>tab</td>
    <td>Jump to the input field below.</td>
  </tr>
</table>

## Display options

{% image workspace/space-center_options.png %}

<table>
  <thead>
    <tr>
      <th width='30%'>option</th>
      <th width='70%'>description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Show Kerning</td>
      <td>
        <p>Toggle display of kerning.</p>
        <p>Only kerning defined in the UFO is shown. Kerning defined in the <code>kern</code> feature is not displayed.</p>
      </td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Suffix ▸</td>
      <td>
        <p>Use glyphs with the selected suffix if available in the font.</p>
        <p>The suffix list is generated from all glyph names containing a dot. For example, if the font contains a glyph named <code>ampersand.alt</code>, the suffix <code>.alt</code> will appear in the list.</p>
      </td>
    </tr>
    <tr>
      <td>Show Layers ▸</td>
      <td>...</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Multi Line</td>
      <td>Break text into multiple lines .</td>
    </tr>
    <tr>
      <td>Single Line</td>
      <td>Display text as a single line.</td>
    </tr>
    <tr>
      <td>Water Fall</td>
      <td>Display a waterfall sample with a range of text sizes.</td>
    </tr>
    <tr>
      <td>xHeight Cut</td>
      <td>Invert foreground and background shapes between baseline and xHeight (ascenders and descenders are clipped). Helps to visualize whitespace when spacing.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Inverse</td>
      <td>Invert foreground and background colors.</td>
    </tr>
    <tr>
      <td>Show Metrics</td>
      <td>Show metrics values for selected glyph.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Left To Right</td>
      <td>Display text from left to right.</td>
    </tr>
    <tr>
      <td>Right To Left</td>
      <td>Display text from right to left.</td>
    </tr>
    <tr>
      <td>Center</td>
      <td>Display text aligned to center.</td>
    </tr>
    <tr>
      <td>Upside Down</td>
      <td>Flip text vertically.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Beam</td>
      <td>Use a beam to measure left and right side-bearings at a given height.</td>
    </tr>
    <tr>
      <td>Guides</td>
      <td>Show font-level guidelines.</td>
    </tr>
    <tr>
      <td>Blues</td>
      <td>Show blue zones.</td>
    </tr>
    <tr>
      <td>Family Blues</td>
      <td>Show family blue zones.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Fill</td>
      <td>Display the fill of the glyph contours.</td>
    </tr>
    <tr>
      <td>Stroke</td>
      <td>Display the stroke of the glyph contours.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Line Space</td>
      <td>Add space between text lines.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Show Control Glyphs</td>
      <td>Toggle the display of control glyphs in the preview text.</td>
    </tr>
    <tr>
      <td>Show Space Matrix</td>
      <td>Toggle the Space Matrix.</td>
    </tr>
  </tbody>
</table>

{% comment %}
## Space Center in action

{% include embed vimeo=47810439 %}
{% endcomment %}
