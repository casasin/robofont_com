---
layout: page
title: Preferences Editor
level: beginner
tags:
  - preferences
---

The Preferences Editor is a special window for editing raw Preferences settings in [json] format.

{% image workspace/preferences-editor.png %}

The Prefences Editor gives access to more settings than available via the {% internallink "preferences-window" %}.

[json]: http://en.wikipedia.org/wiki/JSON

Use the editor to modify the values. When you close the window, you will be asked to save the settings.

> - How are the new settings applied?
{: .todo }
