---
layout: page
title: Glossary
---

{% assign glossaryKeys = "" | split:"|" %}
{% for item in site.data.glossary%}
    {% assign glossaryKeys = glossaryKeys | push: item[0] %}
{% endfor %}
{% assign glossaryKeys = glossaryKeys | sort %}

<dl>
{% for key in glossaryKeys %}
<dt id="{{ key | slugify }}">{{ key }}</dt>
<dd>{{ site.data.glossary[key] | markdownify }}</dd>
{% endfor %}
</dl>
