---
layout: page
title: Sorting glyphs
tags:
  - character set
  - glyph names
level: beginner
---

* Table of Contents
{:toc}

## Sort Glyphs sheet

The glyphs in a font can be sorted using the Sort Glyphs sheet, which can be opened by choosing *Font > Sort* in the {% internallink "workspace/application-menu" %}, or by clicking on the Sort icon in the {% internallink "workspace/font-overview" %} toolbar.

{% image how-tos/sorting-glyphs_sort-icon-toolbar.png %}

The sorting order supplied by the Sort Glyphs sheet is kept in sync with the glyph order in the font, which is stored in the font lib under the key `public.glyphOrder`.

> [lib.plist > Common Key Registry > public.glyphOrder (UFO3 specification)](http://unifiedfontobject.org/versions/ufo3/lib.plist/#publicglyphorder)
{: .seealso }

> The Sort Glyphs sheet applies sorting options to the current font only. To define global sorting options for new fonts, use the {% internallink "preferences-window/sort" %}.
{: .note }

### Smart sort

Use the default sorting algorithm, a combination of matching rules and some preset values.

{% image how-tos/sorting-glyphs_smart-sort.png %}

### Character Set

Use one of your {% internallink "preferences-window/character-set" text="saved Character Sets" %} to sort the font.

{% image how-tos/sorting-glyphs_character-set.png %}

### Custom

Create your own sorting pattern using a set of matching rules.

{% image how-tos/sorting-glyphs_custom.png %}

<table>
  <tr>
    <th width="35%">sort type</th>
    <th width="65%">description</th>
  </tr>
  <tr>
    <td>alphabetical</td>
    <td>Self-explanatory.</td>
  </tr>
  <tr>
    <td>unicode</td>
    <td>Sort based on Unicode value.</td>
  </tr>
  <tr>
    <td>script</td>
    <td>Sort based on Unicode script.</td>
  </tr>
  <tr>
    <td>category</td>
    <td>Sort based on Unicode category.</td>
  </tr>
  <tr>
    <td>block</td>
    <td>Sort based on Unicode block.</td>
  </tr>
  <tr>
    <td>suffix</td>
    <td>Sort based on glyph name suffix.</td>
  </tr>
  <tr>
    <td>decompositionBase</td>
    <td>Sort based on the base glyph defined in the decomposition rules.</td>
  </tr>
  <tr>
    <td>weightedSuffix</td>
    <td>Sort based on glyph names suffix. The ordering of the suffixes is based on the calculated “weight” of the suffix. This value is calculated by noting what type of glyphs have the same suffix. The more glyph types, the more important the suffix. Additionally, glyphs with suffixes that have only numerical differences are grouped together. For example, <code>a.alt</code>, <code>a.alt1</code> and <code>a.alt319</code> will be grouped together.</td>
  </tr>
  <tr>
    <td>ligature</td>
    <td>Sort into to groups: non-ligatures and ligatures. The determination of whether a glyph is a ligature or not is based on the Unicode value, common glyph names or the use of an underscore in the name.</td>
  </tr>
  <tr>
    <td>ascending</td>
    <td>Boolean representing if the glyphs should be in ascending or descending order. Optional. The default is <code>True</code>.</td>
  </tr>
  <tr>
    <td>allowPseudoUnicode</td>
    <td>Boolean representing if pseudo-Unicode values are used. If not, real Unicode values will be used.</td>
  </tr>
</table>

## Sorting glyphs with List mode

The {% internallink "workspace/font-overview" %} window, when in List mode, offers another way to sort glyphs: based on glyph attributes. Just click on a column header to sort glyphs based on the data in that column.

{% image how-tos/sorting-glyphs_list-sort.png caption="click on the column headers to sort glyphs by different attributes" %}

You can use this approach to sort glyphs by color, for example.

> Different than sorting with the Sort Glyphs sheet, sorting with List mode does **not** modify the actual order of glyphs in the font.
{: .note }
