---
layout: page
title: Adding FeatureVariations to variable fonts
tags:
  - variable fonts
  - TTX
level: advanced
draft: true
draft-hidden: true
---

* Table of Contents
{:toc}

## What is FeatureVariations?

FeatureVariations is the none interpolatable variations, A.K.A. Discontinuous variation.
This is about the famous `dollar.nostroke` vs `dollar.withstroke`.

> Small animation of this thing in action ($), one axis!
{: .todo }

In the example the feature is activated if the axis value is greater than `someValue`. This is a one axis example but there can be as many axis involved as your design requires.

> Animation, Example of multi axis. Pan?
{: .todo }

## Make it work.

- create `rvrn` feature
- create lookup
- generate variable OpenType font
- ttx
  - fvar
    - index axes
  - GSUB
    - change version number
    - index feature `rvrn` & lookup
    - add `FeatureVariations` table with correct indexes
- recompile

### Create `rvrn` feature

The registered feature tag for this is `rvrn`. In this feature substitute the activated glyphs by the default glyphs. In the example of the dollar:

```
feature rvrn {
	sub dollar.nostroke by dollar.withstroke;
} rvrn;
```

> [The official OT Feature specs](http://www.microsoft.com/typography/otspec/features_pt.htm#rvrn)
{: .seealso }

### Create the lookup

Also make a `lookup` where the substitutions is reversed. Make up a name for it:

```
lookup DollarNoStrokeActive {
  sub dollar.withstroke by dollar.nostroke;
} DollarNoStrokeActive;
```

### Generate the variable font

Generate the variable font. The FeatureVariations are not working yet.

### TTX

With TTX you can access the font tables (decompile), edit them and recompile the font.

1. Open Terminal
2. Navigate to the directory of your variable font.
3. `ttx -t GSUB -t fvar <path-of-your-otvar.ttf>`
This generates a ttx file with only the `GSUB` and `fvar` tables, these are the only ones we need. Open the ttx file in your favourite code editor.
4. Change the GSUB version number 0x00010000 > 0x0001000`1`
5. Look for the index number of `rvrn` feature. And for the index number of the lookup.

    ``` xml
    …
    <FeatureList>
      …
      <FeatureRecord index="6">
        <FeatureTag value="rvrn"/>
        …
    <LookupList>
      <Lookup index="8">
        <LookupType value="1"/>
        <LookupFlag value="0"/>
        <!-- SubTableCount=1 -->
        <SingleSubst index="0" Format="2">
          <Substitution in="dollar.withstroke" out="dollar.nostroke"/>
        …
    …

    ```

6. add FeatureVariations block
7. with indexes ...
8. recompile

    ``` xml
    <fvar>

        <!-- Weight -->
        <Axis>
          <AxisTag>wght</AxisTag>
          <Flags>0x0</Flags>
          <MinValue>0.0</MinValue>
          <DefaultValue>20.0</DefaultValue>
          <MaxValue>30.0</MaxValue>
          <AxisNameID>256</AxisNameID>
        </Axis>

        <!-- Angle -->
        <Axis>
          <AxisTag>ANGL</AxisTag>
          <Flags>0x0</Flags>
          <MinValue>0.0</MinValue>
          <DefaultValue>180.0</DefaultValue>
          <MaxValue>360.0</MaxValue>
          <AxisNameID>257</AxisNameID>
        </Axis>

        <!-- Steps -->
        <Axis>
          <AxisTag>STEP</AxisTag>
          <Flags>0x0</Flags>
          <MinValue>10.0</MinValue>
          <DefaultValue>20.0</DefaultValue>
          <MaxValue>60.0</MaxValue>
          <AxisNameID>258</AxisNameID>
        </Axis>

        <!-- Style_1 -->
        <NamedInstance flags="0x0" subfamilyNameID="259">
          <coord axis="wght" value="20.0"/>
          <coord axis="ANGL" value="180.0"/>
          <coord axis="STEP" value="20.0"/>
        </NamedInstance>
    </fvar>
    ```

> [Just van Rossum - Robothon 2009, TTX](http://vimeo.com/116064782)
{: .seealso}
