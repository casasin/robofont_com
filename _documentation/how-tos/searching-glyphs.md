---
layout: page
title: Searching glyphs
tags:
  - character set
level: beginner
draft: false
---

* Table of Contents
{:toc}

The {% internallink "workspace/font-overview" %} contains a Search Glyph bar for creating search queries to find glyphs in a font.

{% image how-tos/searching-glyphs_search-bar.png %}

In {% internallink "workspace/window-modes#multi-window-mode" text="Multi-Window mode" %}, users can toggle the Search Glyph bar with the View buttons in the Font Overview’s toolbar:

{% image how-tos/searching-glyphs_views-buttons-multi.png %}

In {% internallink "workspace/window-modes#single-window-mode" text="Single Window mode" %}, the View buttons are located at the bottom of the window:

{% image how-tos/searching-glyphs_views-buttons-single.png %}

In both modes, the Search and Find bar can also be toggled with the keyboard shortcut ⌘ + F.

## Search expressions

A search query is made out of one or more search expressions, combined with a boolean operator. Search expressions can be built using different types of glyph data and conditions.

To add a new search expression to the query, click on the + at the right of the Search Glyph bar.

### Glyph data

The types of glyph data supported by search expressions are the same which are shown in the header of the table of the {% internallink "workspace/font-overview#list-mode" text="Font Overview in List Mode" %}.

<table>
  <tr>
    <th width='30%'>attribute</th>
    <th width="45%">description</th>
    <th width="25%">type</th>
  </tr>
  <tr>
    <td>Name</td>
    <td>Glyph name.</td>
    <td>string</td>
  </tr>
  <tr>
    <td>Width</td>
    <td>Glyph width.</td>
    <td>integer or float</td>
  </tr>
  <tr>
    <td>Left sidebearing</td>
    <td>Glyph left sidebearing.</td>
    <td>integer or float</td>
  </tr>
  <tr>
    <td>Right sidebearing</td>
    <td>Glyph right sidebearing.</td>
    <td>integer or float</td>
  </tr>
  <tr>
    <td>Unicode</td>
    <td>Glyph unicode.</td>
    <td>hex</td>
  </tr>
  <tr>
    <td>Contours</td>
    <td>Amount of contours in the glyph.</td>
    <td>integer</td>
  </tr>
  <tr>
    <td>Components</td>
    <td>Amount of components in the glyph.</td>
    <td>integer</td>
  </tr>
  <tr>
    <td>Anchors</td>
    <td>Amount of anchors in the glyph.</td>
    <td>integer</td>
  </tr>
  <tr>
    <td>Components names</td>
    <td>A comma-separated list of the base glyph names of all components in the glyph.</td>
    <td>list</td>
  </tr>
  <tr>
    <td>Anchor names</td>
    <td>A comma-separated list of all anchor names in the glyph.</td>
    <td>list</td>
  </tr>
  <tr>
    <td>Note</td>
    <td>Glyph note.</td>
    <td>string</td>
  </tr>
  <tr>
    <td>Empty</td>
    <td>A bool indicating if the glyph is empty.</td>
    <td>bool</td>
  </tr>
  <tr>
    <td>Glyph changed</td>
    <td>A bool indicating if the glyph has been changed since the last saved version.</td>
    <td>bool</td>
  </tr>
  <tr>
    <td>Template</td>
    <td>A bool indicating if the glyph is a template glyph.</td>
    <td>bool</td>
  </tr>
  <tr>
    <td>Mark Color</td>
    <td>The mark color of the glyph.</td>
    <td>color</td>
  </tr>
</table>

### Conditions

Each type of data offers a set of filtering conditions which can be used in the search expression.

<table>
  <tr>
    <th width='40%'>type</th>
    <th width="60%">conditions</th>
  </tr>
  <tr>
    <td>string</td>
    <td>
      <ul>
        <li>contains</li>
        <li>begins with</li>
        <li>ends with</li>
        <li>matches</li>
        <li>is</li>
        <li>is not</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>number</td>
    <td>
      <ul>
        <li>is less than</li>
        <li>is</li>
        <li>is greater than</li>
        <li>is not</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>list</td>
    <td>
      <ul>
        <li>contains</li>
        <li>is not</li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>bool</td>
    <td>
      <ul>
        <li>is</li>
        <li>is not</li>
      </ul>
    </td>
  </tr>
    <td>color</td>
    <td>
      <ul>
        <li>is</li>
        <li>is not</li>
        <li>contains</li>
        <li>matches</li>
      </ul>
    </td>
</table>

### Boolean operators

Search expressions can be combined using boolean operators.

{% image how-tos/searching-glyphs_boolean-filter.png %}

<table>
  <tr>
    <th width='20%'>options</th>
    <th width='20%'>operator</th>
    <th width="60%">result</th>
  </tr>
  <tr>
    <td>All</td>
    <td>AND</td>
    <td>Show glyphs which match all queries.</td>
  </tr>
  <tr>
    <td>Any</td>
    <td>OR</td>
    <td>Show glyphs which match at least one query.</td>
  </tr>
  <tr>
    <td>None</td>
    <td>NOT</td>
    <td>Show glyphs which don’t match any query.</td>
  </tr>
</table>

### Regular expressions

The *matches* condition (available for glyph names and mark colors) supports regular expressions. Regex is useful for creating smarter, more compact search expressions.

Examples:

`[a-z]`
: Lists all lowercase glyphs.

`[A-Z]`
: Lists all uppercase glyphs.

`[A-z]`
: Lists all 52 uppercase and lowercase glyphs.

`[a-z](caron|cedilla|ogonek|commaaccent|grave|acute|dieresis)`
: Lists all lowercase glyphs containing these accents.

<!--
find out what this does exactly: ([A-Z]*)
-->

## Saving Search Queries as Smart Sets

Search queries can be saved as Query-based or List-based Smart Sets using the two buttons at the top of the Search Glyphs bar.

{% image how-tos/searching-glyphs_queries-save.png %}

<table>
  <tr>
    <th width="25%">option</th>
    <th width="75%">description</th>
  </tr>
  <tr>
    <td>Selection to Set</td>
    <td>
      <p>Save the selected glyphs as a <a href="../../workspace/smart-sets-panel#creating-list-based-smart-sets">List-based Smart Set</a>.</p>
      <p>List-based Smart Sets display glyphs based on a static list of glyph names.</p>
    </td>
  </tr>
  <tr>
    <td>Save Set</td>
    <td>
      <p>Save the current query as a <a href="../../workspace/smart-sets-panel#creating-query-based-smart-sets">Query-based Smart Set</a>.</p>
      <p>Query-based Smart Sets search the font dynamically, so the result is updated based on changes in the font.</p>
    </td>
  </tr>
</table>
