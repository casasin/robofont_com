---
layout: page
title: Creating designspaces
tags:
  - variable fonts
  - interpolation
  - designspace
level: intermediate
draft: true
---

* Table of Contents
{:toc}

## The .designspace file format

A `.designspace` file is an XML-based description of a multi-dimensional interpolation space. It contains information about several objects which make up the designspace:

axes
: name, tag, mininum/maximum/default values, labels

masters
: UFO path, familyName, styleName, layer, location

instances
: UFO path, familyName, styleName, location

rules
: name, substitutions, conditionSets, conditions

> Designspace files were introduced by [MutatorMath], and are now used by [varLib] too.
{: .note }

> - [DesignSpaceDocument > Document xml structure](http://github.com/LettError/designSpaceDocument#document-xml-structure)
{: .seealso }

## Creating designspaces with Superpolator

[Superpolator] is a stand-alone application to design interpolation spaces. It provides an interactive UI with a designspace map, interpolation preview, location editor, rules editor etc.

Superpolator’s native data format is `.sp3`, but it can also read and write `.designspace` files.

{% image how-tos/creating-designspace-files_Superpolator.png %}

1. Define your design space using Superpolator’s interface.
2. Choose *File > Export DesignSpace…* to export the data to a `.designspace` file.

> - [Superpolator documentation]
{: .seealso }

> Superpolator requires a paid license for continuous use. A fully functional demo version is also available, and expires after 30 days.
{: .note }

## Creating designspaces with DesignSpaceEditor

[DesignSpaceEditor] is a RoboFont extension which provides a simple interface to create and edit designspace data – without any interactive preview, map etc.

{% image how-tos/creating-designspace-files_DesignSpaceEditor.png %}

1. Design your designspace using DesignSpaceEditor’s interface.
2. Click on the *Save* button to save the data to a `.designspace` file.

> - [DesignSpaceEditor documentation][DesignSpaceEditor]
{: .seealso }

> DesignSpaceEditor is open-source and can be {% internallink 'extensions/installing-extensions-mechanic' text='installed with Mechanic 2' %}.
{: .note }

## Creating designspaces with designSpaceLib

Designspace files can also be created with code: {% glossary fontTools %} supports reading & writing `.designspace` files using the [designSpaceLib].

> DesignspaceLib started as [DesignSpaceDocument], a separate library, and was recently incorporated into fontTools.
{: .note }

The script below will create a `.designspace` file for MutatorSans. To try it out, save it in the MutatorSans folder next to the UFOs.

{% showcode how-tos/makeDesignSpace.py %}

> - [designSpaceLib documentation][designSpaceLib]
{: .seealso }

[designSpaceLib]: http://github.com/fonttools/fonttools/blob/master/Doc/source/designspaceLib/scripting.rst
[designSpaceDocument]: http://github.com/LettError/designSpaceDocument
[DesignSpaceEditor]: http://github.com/LettError/designSpaceRoboFontExtension
[MutatorMath]: http://github.com/LettError/mutatorMath
[Superpolator]: http://superpolator.com/
[Superpolator documentation]: http://superpolator.com/documentation/
[varLib]: http://github.com/fonttools/fonttools/tree/master/Lib/fontTools/varLib
