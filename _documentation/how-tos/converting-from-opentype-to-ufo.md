---
layout: page
title: Converting from OpenType to UFO
tags:
  - UFO
  - OpenType
level: intermediate
draft: true
---

* Table of Contents
{:toc}

## Opening OpenType fonts

{% glossary OpenType fonts %} can be opened in RoboFont in the same way as {% glossary UFO %} fonts: with the *Open Font* dialog, or with a script:

```python
f = OpenFont('myFont.otf')
```

## What is imported

### Glyph data

from the `glyf` and `cff ` tables

### Kerning & Groups

from the `kern` table

### Font Info

from various tables:

- `head`
- `name`
- `OS/2`
- `hhea`
- `post`
- `gasp`

(see [extractor](http://github.com/typesupply/extractor/blob/master/Lib/extractor/formats/opentype.py))

## What is not imported

### OpenType features

OpenType features are present only in compiled form and cannot be imported (readably) from binary fonts.

### etc

...

## Saving to UFO

To save the opened OpenType font as UFO, use the *Save* dialog – just as you would do with UFO fonts.

### UFO lib key

RoboFont adds a key to `font.lib` with the path of the original binary source:

    com.typemytype.robofont.binarySource

> This is already used by the Batch extension.
{: .note }
