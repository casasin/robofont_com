---
layout: page
title: Building glyphs with Glyph Construction
tags:
  - accents
level: intermediate
---

* Table of Contents
{:toc}

[Glyph Construction] is a language to describe how shapes are constructed. It allows one to create new glyphs by adding multiple glyphs as components, with options for positioning each component in relation to the base glyph and to any other component.

Glyph Construction offers a simple and human-readable syntax which makes creating accented glyphs very easy.

[Glyph Construction]: http://github.com/typemytype/GlyphConstruction

## Downloading Glyph Construction

Glyph Construction is still in beta stage, and is available only as a script.

> Turn Glyph Construction into an extension, so it is easily available as UI-based tool and as a Python module (for use by scripts). See [#14](http://github.com/typemytype/GlyphConstruction/pull/14).
{: .todo }

To use Glyph Construction, you’ll need to download the source files from the project’s repository on [GitHub][Glyph Construction]. After the download is completed, unzip the package, and save the files somewhere in your machine.

## Creating new glyphs with Glyph Builder

To use Glyph Construction in RoboFont, open the Glyph Builder by running the script `glyphConstructionUI.py` in the {% internallink "scripting-window" %}.

{% image how-tos/building-accented-glyphs_glyph-builder.png caption="the Glyph Builder interface" %}

The Glyph Builder’s interface includes the following components:

Toolbar
: The toolbar at the top of the window contains icons for triggering several actions. See the [table below](#actions) for a description of each action.

Rules
: ^
  The left column contains a simple text editor where glyph construction rules are written, one per line.

  Glyph construction rules are written in the Glyph Construction language. See the project’s [GitHub][Glyph Construction] page for syntax documentation and examples.

Preview
: ^
  The middle column shows a preview for every glyph defined in the first column.

  The glyph display is provided by same `MultiLineView` component used by the Space Center, and supports zooming in/out with ⌘ +/- keys or with the mouse.

  Individual glyphs can be selected with a click. See *Preview selected* below.

Analysis
: The right column displays useful information about all glyphs defined in the first column: missing glyphs, existing glyphs, existing glyphs with missing components, and existing glyphs with different components.

Preview selected
: ^
  Displays a preview of the glyph which is currently selected in the Preview area.

  If the glyph already exists in the font, its current shape is displayed in the background, in red.

Status bar
: The status bar at the bottom of the window shows some information about the selected glyph: glyph name, width, left & right margins, component names, unicode, mark color, and note.

### Actions

<table>
  <thead>
    <tr>
      <th width="25%">option</th>
      <th width="75%">description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Save</td>
      <td>Save all rules as a <code>.glyphConstruction</code> file.</td>
    </tr>
    <tr>
      <td>Open</td>
      <td>Open an existing <code>.glyphConstruction</code> file.</td>
    </tr>
    <tr>
      <td>Update</td>
      <td>Update the contents of the Preview and Analysis columns.</td>
    </tr>
    <tr>
      <td>Analyse</td>
      <td>Show/hide the Analysis column.</td>
    </tr>
    <tr>
      <td>Build Glyphs</td>
      <td>Build all described glyphs in the current font.</td>
    </tr>
  </tbody>
  <tbody>
    <tr>
      <td>Build</td>
      <td>Build only the selected glyph.</td>
    </tr>
  </tbody>
</table>

## Examples

### Accented glyphs

The example below shows a real-world application of Glyph Construction syntax for adding accented glyphs to an existing font.

```text
aacute = a + acute@center,top | 00E1
abreve = a + breve@center,top | 0103
acircumflex = a + circumflex@center,top | 00E2
adieresis = a + dieresis@center,top | 00E4
aeacute = ae + acute@center,top | 01FD
agrave = a + grave@center,top | 00E0
amacron = a + macron@center,top | 0101
aogonek = a + ogonek@ogonek,origin | 0105
aring = a + ring@center,top | 00E5
aringacute = a + ring@center,top + acute@center,top | 01FB
atilde = a + tilde@center,top | 00E3
```

> See the `examples` folder included the repository for the complete list of glyph construction rules for accented glyphs.
{: .note }

### Modular glyphs

The following example shows a more experimental use of Glyph Construction, for creating modular shapes. It demonstrates the construction of a new glyph `o` out of multiple copies of a `pixel` glyph.

```text
o = pixel + pixel@center,top + pixel@center,top + pixel@center,top + pixel@right,top + pixel@right,center + pixel@right,bottom + pixel@center,bottom + pixel@center,bottom + pixel@center,bottom + pixel@left,bottom + pixel@left,center
```

> To try out this example, create a font containing only one glyph named `pixel`, and draw a simple shape inside it:
>
> ```python
> f = NewFont()
> f.newGlyph('pixel')
> pen = f['pixel'].getPen()
> pen.moveTo((0, 0))
> pen.lineTo((0, 100))
> pen.lineTo((100, 100))
> pen.lineTo((100, 0))
> pen.closePath()
> ```
{: .note }

> - [Glyph Construction Demo (RoboThon 2015)](http://vimeo.com/123781567#t=24m13s)
{: .seealso }
