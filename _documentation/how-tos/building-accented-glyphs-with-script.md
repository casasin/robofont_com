---
layout: page
title: Building accented glyphs with code
tags:
  - accents
  - scripting
level: intermediate
draft: false
---

> This example works only in RF1 (RoboFab API) – RF3 (FontParts API) does not include a `font.compileGlyph` method.
{: .warning }

> - [font.compileGlyph is missing](http://github.com/robofab-developers/fontParts/issues/81)
> - {% internallink 'how-tos/converting-accents-dict-to-glyph-construction' %}
> - {% internallink 'how-tos/building-glyphs-with-glyph-construction' %}
{: .seealso }

Like most font production tasks in RoboFont, the creation of accented glyphs can also be automated with code. The script below shows a simple accent builder example.

{% showcode how-tos/accentBuilder.py %}
