---
layout: page
title: Converting from UFO2 to UFO3
tags:
  - UFO
level: beginner
draft: true
---

RoboFont 3 opens [UFO2] files natively and converts them to [UFO3] using the [ufoLib].

> - If opening a UFO2 file fails, RoboFont runs [ufoNormalizer] on it first, and then converts the normalized font to UFO3.
> - Both RoboFont and ufoLib provide reports on where the conversion process failed.
{: .note }

## Saving a font to any UFO version

To save an open font as UFO1, UFO2 or UFO3, use the menu *File > Save As…* to open a sheet and choose the desired format from the list.

> - {% internallink 'introduction/the-ufo-format' %}
{: .seealso }

[UFO2]: http://unifiedfontobject.org/versions/ufo2/
[UFO3]: http://unifiedfontobject.org/versions/ufo3/
[ufoLib]: http://github.com/unified-font-object/ufoLib
[ufoNormalizer]: http://github.com/unified-font-object/ufoNormalizer
