---
layout: page
title: Converting accentsDict to Glyph Construction
tags:
  - accents
  - scripting
level: intermediate
---

The RoboFont 3/ FontParts API does not include the `font.compile()` method for building accented glyphs.

Convert a dictionary with data for building accented glyphs into a list of Glyph Construction rules.

> - [Glyph Construction](http://github.com/typemytype/GlyphConstruction)
> - {% internallink 'how-tos/building-glyphs-with-glyph-construction' %}
{: .seealso }

{% showcode how-tos/accentsDict2GlyphConstruction.py %}
