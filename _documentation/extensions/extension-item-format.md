---
layout: page
title: Extension Item Format
tags:
  - extensions
  - Mechanic
level: intermediate
---

* Table of Contents
{:toc}

## Extension Item Specification

An *Extension Item* is a simple dictionary with data about an extension.

The following key/value pairs are supported:

<table>
<tr>
  <th width='35%'>key</th>
  <th>value</th>
</tr>
{% for data in site.data.extensionItemSpec.extensionItem %}
<tr>
  <td rowspan='3'>
    <code class='highlighter-rouge'>{{ data.key }}</code>
  </td>
  <td>{{ data.description | markdownify }}</td>
</tr>
<tr>
  <td><code>{{ data.value }}</code></td>
</tr>
<tr>
  <td class="{{ data.required | slugify }}">{{ data.required | markdownify  }}</td>
</tr>
{% endfor %}
</table>

### Extension icons

Mechanic 2 supports optional extension icons, which are displayed in the extensions overview list if available. Icons make it easier to identify and remember extensions.

Here are some additional recommendations for icon images:

<table>
  <tr>
    <th width='25%'>attribute</th>
    <th width='75%'>recommendation</th>
  </tr>
  <tr>
    <td>file path</td>
    <td>save the image outside of the <code>.roboFontExt</code> package</td>
  </tr>
  <tr>
    <td>file name</td>
    <td><code>&lt;extensionName&gt;MechanicIcon.png</code></td>
  </tr>
  <tr>
    <td>format</td>
    <td><code>.png</code> with transparent background</td>
  </tr>
  <tr>
    <td>size</td>
    <td>512 × 512 px</td>
  </tr>
</table>

## Usage

Extension items can be distributed by themselves or collected into *extension streams*.

The extension item format is not tied to a particular data format:

- single extension items are stored in [yaml] format
- extension streams are served as [json] data

[yaml]: http://en.wikipedia.org/wiki/YAML
[json]: http://en.wikipedia.org/wiki/JSON

### Extension items

Here is an example extension item in `yaml` format:

```yaml
extensionName: myExtension
repository: http://github.com/robodocs/rf-extension-boilerplate
extensionPath: myExtension.roboFontExt
description: A boilerplate extension which serves as starting point for creating your own extensions.
developer: RoboDocs
developerURL: http://github.com/roboDocs
tags: [ demo ]
infoPath: http://github.com/roboDocs/rf-extension-boilerplate/blob/master/build/myExtension.roboFontExt/info.plist?private_token=myPrivateToken
zipPath: http://github.com/roboDocs/rf-extension-boilerplate/archive/master.zip?private_token=myPrivateToken
icon: http://github.com/roboDocs/rf-extension-boilerplate/blob/master/build/myExtension.roboFontExt/resources/icon.png?private_token=myPrivateToken
```

Single extension items can be stored under different filenames, depending on the desired distribution channel:

`<extensionName>.yml`
: for registration in the Mechanic 2 extension stream

`<extensionName>.mechanic`
: for distribution as single extension item

> - {% internallink 'extensions/publishing-extensions' %}
{: .seealso }

### Extension streams

An *extension stream* is a `.json` file containing a list of extension items and a date (when the list was last updated).

Here is an example extension stream:

```json
{
  "lastUpdate" : "2018-06-08 21:21",
  "extensions" : [
    {
      "extensionName": "myExtension",
      "repository" :  "http://github.com/robodocs/rf-extension-boilerplate",
      "extensionPath" : "build/myExtension.roboFontExt",
      "description" : "A boilerplate extension which serves as starting point for creating your own extensions.",
      "developer" : "RoboDocs",
      "developerURL" : "http://github.com/roboDocs",
      "tags" : ["demo"],
      "icon" : "http://github.com/roboDocs/rf-extension-boilerplate/blob/master/build/myExtension.roboFontExt/resources/icon.png"
    },
    /* ... more extensions here ... */
  ]
}
```

Extension streams can be created dynamically (by a server) by aggregating data from several single extension item files. See the [Mechanic 2 extension stream] for an example.

[Mechanic 2 extension stream]: http://robofontmechanic.com/api/v2/registry.json

> - {% internallink 'extensions/managing-extension-streams' %}
{: .seealso }

## Private access tokens

Extensions in private repositories are supported as long as a valid private token is provided. This private token is needed to access individual files inside the extension’s repository: `infoPath`, `zipPath`, and `icon` (if available).

The `yaml` extension item sample [above](#extension-items) shows private tokens in use – search for occurrences of `private_token`.

### Creating private access tokens

Private tokens can be created using the web interface of your favorite Git platform:

- [New personal access token (GitHub)](http://github.com/settings/tokens/new)
- [Personal access tokens (GitLab)](http://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
- [App passwords (BitBucket)](http://confluence.atlassian.com/bitbucket/app-passwords-828781300.html)
