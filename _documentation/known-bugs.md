---
layout: page
title: Known bugs
tags:
  - bugs
---

* Table of Contents
{:toc}

## Cursor size

where does it happen
: macOS 10.13 + Retina display

problem description
: In *System Preferences > Accessibility > Display > Cursor size*, anything other than the Normal cursor size will make the cursor appear with the wrong size in the {% internallink 'workspace/glyph-editor' %}.

workaround
: Use the Normal cursor size.
