---
layout: page
title: Workspace
tree:
  - application-menu
  - window-modes
  - font-overview
  - glyph-editor
  - inspector
  - space-center
  - kern-center
  - font-info-sheet
  - preferences-window
  - features-editor
  - groups-editor
  - smart-sets-panel
  - scripting-window
  - output-window
  - extension-builder
  - keyboard-shortcuts
  - preferences-editor
  - about-window
  - license-window
level: beginner
---

**What RoboFont looks like. A description of its menus, windows, panels etc.**

{% tree page.url levels=1 %}

{% comment %}
TO-DO:
- update screencasts + create new ones
- create gif sequences
{% endcomment %}
